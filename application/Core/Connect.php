<?php

namespace Application\Core;

use Application\Config\Config;
use Application\Helpers\Functions;

/**
 * Class Connect
 * @package Application\Core
 */
class Connect
{
    /**
     *
     */
    private const OPTIONS = [
        \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
        \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_OBJ,
        \PDO::ATTR_CASE => \PDO::CASE_NATURAL
    ];

    /**
     * @var
     */
    private static $instance;

    /**
     * @return \PDO|null
     */
    public static function getInstance(): ?\PDO
    {
        if(empty(self::$instance)){
            try{
                self::$instance = new \PDO(
                    'mysql:host=' . Config::DATABASE_HOST . ';dbname=' . Config::DATABASE_NAME,
                    Config::DATABASE_USER,
                    Config::DATABASE_PASSWORD,
                    self::OPTIONS
                );
            }catch (\PDOException $exception){
                var_dump($exception);
                //Functions::redirect('/ops/problemas');
            }
        }
        return self::$instance;
    }

    /**
     * Connect constructor.
     */
    final private function __construct()
    {
    }

    /**
     * Connect clone
     */
    final private function __clone()
    {
    }
}