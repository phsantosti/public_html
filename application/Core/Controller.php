<?php


namespace Application\Core;


use Application\Libraries\Message;
use Application\Libraries\Seo;

class Controller
{
    protected $view;
    protected $seo;
    protected $message;

    public function __construct(string $pathToViews = null)
    {
        $this->view = new View($pathToViews);
        $this->seo = new Seo();
        $this->message = new Message();
    }
}