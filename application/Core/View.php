<?php


namespace Application\Core;


use Application\Config\Config;
use League\Plates\Engine;

class View
{
    private $engine;

    public function __construct(string $path = Config::VIEW_PATH, string $ext = Config::VIEW_EXTENSION)
    {
        $this->engine = Engine::create($path, $ext);
    }

    public function path(string $name, string $path): View
    {
        $this->addFolder($name, $path);
        return $this;
    }

    public function render(string $templateName, array $data): string
    {
        return $this->engine->render($templateName,$data);
    }

    public function engine(): Engine{
        return $this->engine();
    }
}