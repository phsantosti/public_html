<?php


namespace Application\Libraries;


use Application\Config\Config;
use CoffeeCode\Cropper\Cropper;

class Thumb
{
    /** @var Cropper */
    private $cropper;

    /** @var string */
    private $uploads;

    /**
     * Thumb constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->cropper = new Cropper(Config::IMAGE_CACHE, Config::IMAGE_QUALITY['jpg'], Config::IMAGE_QUALITY['png']);
        $this->uploads = Config::UPLOAD_BASE_PROJECT_DIR . "/" . Config::UPLOAD_DIR;
    }

    /**
     * @param string $image
     * @param int $width
     * @param int|null $height
     * @return string
     */
    public function make(string $image, int $width, ?int $height = null): string
    {
        return $this->cropper->make("{$this->uploads}/{$image}", $width, $height);
    }

    /**
     * @param string|null $image
     */
    public function flush(?string $image = null): void
    {
        if ($image) {
            $this->cropper->flush("{$this->uploads}/{$image}");
            return;
        }

        $this->cropper->flush();
        return;
    }

    /**
     * @return Cropper
     */
    public function cropper(): Cropper
    {
        return $this->cropper;
    }
}