<?php


namespace Application\Libraries;


use Application\Config\Config;
use CoffeeCode\Optimizer\Optimizer;

class Seo
{
    protected $optimizer;

    public function __construct(string $schema = "article")
    {
        $this->optimizer = new Optimizer();
        $this->optimizer->openGraph(Config::SITE_NAME, Config::SITE_LANGUAGE, $schema);
        $this->optimizer->twitterCard(Config::SOCIAL_TWITTER_CREATOR, Config::SOCIAL_TWITTER_PUBLISHER, Config::SITE_DOMAIN);
        $this->optimizer->publisher(Config::SOCIAL_FACEBOOK_PAGE, Config::SOCIAL_FACEBOOK_AUTHOR, Config::SOCIAL_GOOGLE_PAGE, Config::SOCIAL_GOOGLE_AUTHOR);
        $this->optimizer->facebook(Config::SOCIAL_FACEBOOK_APP);
    }

    public function __get($name)
    {
        return $this->optimizer->data()->$name;
    }

    public function render(string $title, string $description, string $url, string $image, bool $follow = true): string
    {
        return $this->optimizer->optimize($title, $description, $url, $image, $follow)->render();
    }

    public function optimizer(): Optimizer
    {
        return $this->optimizer;
    }

    public function data(?string $title = null, ?string $description = null, ?string $url = null, ?string $image = null)
    {
        return $this->optimizer->data($title, $description, $url, $image);
    }
}