<?php


namespace Application\Controllers\Site;

use Application\Config\Config;
use Application\Core\Controller;
use Application\Helpers\Functions;

class EbookController extends Controller
{
    public function __construct()
    {
        parent::__construct(__DIR__ . '/../../../themes/' . Config::VIEW_THEME . '/');

    }

    public function index(): void
    {
        $data = [
            'head' => $this->seo->render(
                Config::SITE_NAME . ' - ' . Config::SITE_TITLE,
                Config::SITE_DESCRIPTION,
                Functions::url(),
                Functions::theme("/assets/images/share.jpg")
            )
        ];
        //var_dump(true);
        echo $this->view->render("ebooks/ebooks", $data);
    }
}