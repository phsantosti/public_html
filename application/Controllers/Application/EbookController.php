<?php


namespace Application\Controllers\Application;


use Application\Config\Config;
use Application\Core\Controller;
use Application\Helpers\AppFunctions;
use Application\Helpers\Functions;
use Application\Libraries\Thumb;
use Application\Libraries\Upload;
use Application\Models\EbookModel;

class EbookController extends Controller
{
    public function __construct()
    {
        parent::__construct(__DIR__ . "/../../../../public_html/themes/" . Config::VIEW_ADMIN . "/");
        AppFunctions::checkUserLogged();
    }

    public function index(): void
    {
        $data = [
            'page_name' => 'E-books',
            'head' => $this->seo->render(
                "E-books | CoreCMS - Gerenciador de Conteúdo Web Modular",
                "Você no controle das suas informações",
                AppFunctions::appUrl(),
                AppFunctions::appTheme("/assets/images/appShare.jpg", Config::VIEW_ADMIN),
                false
            ),
        ];
        echo $this->view->render("widgets/ebooks/home", $data);
    }

    public function ebookFiles(): void{
        $data = [
            'page_name' => 'E-book - Arquivos',
            'head' => $this->seo->render(
                "E-book - Arquivos | CoreCMS - Gerenciador de Conteúdo Web Modular",
                "Você no controle das suas informações",
                AppFunctions::appUrl(),
                AppFunctions::appTheme("/assets/images/appShare.jpg", Config::VIEW_ADMIN),
                false
            )
        ];
        echo $this->view->render("widgets/ebooks/files", $data);
    }

    public function ebookReadFile(?array $data){
        if(!empty($data['ebook_id'])){
            if(Functions::user()->level < 3){
                $json = [
                    'success' => false,
                    'title' => 'Ops!',
                    'message' => 'Você não tem permissão para executar esta ação'
                ];
                echo json_encode($json);
                return;
            }

            $ebook = (new EbookModel())->findById($data['ebook_id']);
            if(!empty($ebook)){
                $e = new \stdClass();
                $e->id = $ebook->id;
                $e->title = $ebook->title;
                $e->description = $ebook->description;
                $e->image = "<img src='".AppFunctions::image($ebook->image, 94, 119)."' class='img-fluid' alt='".$ebook->title."' title='".$ebook->title."'/>";
                $e->link = "<a href='".Functions::url('/storage/'.$ebook->link)."' target='_blank' class='btn btn-primary' download data-toggle='tooltip' data-placement='left' data-original-title='Efetuar download do arquivo' title><i class='icon-cloud-download'></i></a>";
                $e->downloads = $ebook->downloads;
                $e->status = $ebook->status;
                $e->created_at = $ebook->created_at;
                $e->updated_at = $ebook->updated_at;

                $json = [
                    'success' => true,
                    'title' => 'Sucesso',
                    'message' => 'E-book localizado...',
                    'ebook' => $e
                ];
                echo json_encode($json);
                return;
            }else{
                $json = [
                    'success' => false,
                    'title' => 'Ops!',
                    'message' => 'O E-book que está tentando editar não existe mais, atualize a página para recarregar a lista de e-books',
                    'ebook' => null
                ];
                echo json_encode($json);
                return;

            }
        }else{
            $ebooks = (new EbookModel())->find()->fetch(true);
            $arrayEbooks = [];
            if(!empty($ebooks)){
                foreach ($ebooks as $ebook){
                    $e = new \stdClass();
                    $e->id = $ebook->id;
                    $e->title = $ebook->title;
                    $e->link = "<a href='".Functions::url('/storage/'.$ebook->link)."' target='_blank' class='btn btn-primary' download data-toggle='tooltip' data-placement='left' data-original-title='Efetuar download do arquivo' title><i class='icon-cloud-download'></i></a>";
                    $e->downloads = $ebook->downloads;
                    $e->status = ($ebook->status == 1 ? "Ativo" : "Inativo");
                    $e->created_at = Functions::dateFormatBraazil($ebook->created_at);
                    $e->updated_at = Functions::dateFormatBraazil($ebook->updated_at);
                    $e->options =
                        "<button data-toggle='tooltip' data-placement='left' data-original-title='Editar E-book' title type='button' class='btn btn-primary text-white font-weight-bold' id='".$ebook->id."' onclick='editEbook(this.id);'><i class='icon-note'></i></button> ".
                        "<button data-toggle='tooltip' data-placement='left' data-original-title='Deletar E-book' title type='button' class='btn btn-danger text-white font-weight-bold' id='".$ebook->id."' onclick='deleteEbook(this.id);'><i class='icon-close'></i> </button> ";

                    array_push($arrayEbooks, $e);
                }

                $json['data'] = $arrayEbooks;
                echo json_encode($json);
                return;
            }
        }
    }

    public function ebookCreateFile(?array $data){
        if(!empty($data)){
            if(Functions::user()->level < 3){
                $json = [
                    'success' => false,
                    'title' => 'Ops!',
                    'message' => 'Você não tem permissão para executar esta ação'
                ];
                echo json_encode($json);
                return;
            }

            if(empty($data['title'])){
                $json = [
                    'success' => false,
                    'title' => 'Ops!',
                    'message' => 'O título do E-book deve ser informado'
                ];
                echo json_encode($json);
                return;
            }

            if(empty($data['description'])){
                $json = [
                    'success' => false,
                    'title' => 'Ops!',
                    'message' => 'A descrição do E-book deve ser informada'
                ];
                echo json_encode($json);
                return;
            }

            $image = "";
            if(empty($_FILES['image'])){
                $json = [
                    'success' => false,
                    'title' => 'Ops!',
                    'message' => 'A imagem de capa do ebook deve ser enviada'
                ];
                echo json_encode($json);
                return;
            }else{
                $files = $_FILES['image'];
                $upload = new Upload();
                $image = $upload->image($files, Functions::stringSlug($data['title']), 373);
                if(!$image){
                    $json = [
                        'success' => false,
                        'title' => 'Ops!',
                        'message' => $upload->message()->render()
                    ];
                    echo json_encode($json);
                    return;
                }
            }

            $link = "";
            if(empty($_FILES['link'])){
                $json = [
                    'success' => false,
                    'title' => 'Ops!',
                    'message' => 'O arquivo PDF deve ser informado'
                ];
                echo json_encode($json);
                return;
            }else{
                $files = $_FILES['link'];
                $upload = new Upload();
                $link = $upload->file($files,Functions::stringSlug($data['title']));
                if(!$link){
                    $json = [
                        'success' => false,
                        'title' => 'Ops!',
                        'message' => $upload->message()->render()
                    ];
                    echo json_encode($json);
                    return;
                }
            }

            $ebook = new EbookModel();
            $ebook->bootstrap(Functions::stringTitle($data['title']), $data['description'],$image,$link,0,$data['status']);
            if(!$ebook->save()){
                $json = [
                    'success' => false,
                    'title' => 'Ops!',
                    'message' => $ebook->message()->render()
                ];
                echo json_encode($json);
                return;
            }

            $this->message->success("E-book cadastrado com sucesso...")->flash();
            $json = [
                'success' => true,
                'title' => 'Sucesso',
                'message' => 'E-book cadastrado com sucesso...',
                'redirect' => AppFunctions::appUrl('/ebooks/files')
            ];
            echo json_encode($json);
            return;
        }
    }

    public function ebookUpdateFile(?array $data){
        if(!empty($data['ebook_id'])){
            if(Functions::user()->level < 3){
                $json = [
                    'success' => false,
                    'title' => 'Ops!',
                    'message' => 'Você não tem permissão para executar esta ação'
                ];
                echo json_encode($json);
                return;
            }

            if(empty($data['title'])){
                $json = [
                    'success' => false,
                    'title' => 'Ops!',
                    'message' => 'O título do E-book deve ser informado'
                ];
                echo json_encode($json);
                return;
            }

            if(empty($data['description'])){
                $json = [
                    'success' => false,
                    'title' => 'Ops!',
                    'message' => 'A descrição do E-book deve ser informada'
                ];
                echo json_encode($json);
                return;
            }

            $image = "";
            if(!empty($_FILES['image'])){
                $files = $_FILES['image'];
                $upload = new Upload();
                $image = $upload->image($files, Functions::stringSlug($data['title']), 373);
                if(!$image){
                    $json = [
                        'success' => false,
                        'title' => 'Ops!',
                        'message' => $upload->message()->render()
                    ];
                    echo json_encode($json);
                    return;
                }
            }

            $link = "";
            if(!empty($_FILES['link'])){
                $files = $_FILES['link'];
                $upload = new Upload();
                $link = $upload->file($files,Functions::stringSlug($data['title']));
                if(!$link){
                    $json = [
                        'success' => false,
                        'title' => 'Ops!',
                        'message' => $upload->message()->render()
                    ];
                    echo json_encode($json);
                    return;
                }
            }

            $ebook = (new EbookModel())->findById($data['ebook_id']);

            if(!empty($ebook)){
                $ebook->title = Functions::stringTitle($data['title']);
                $ebook->description = $data['description'];

                if(!empty($image)){
                    $ebook->image = $image;
                }

                if(!empty($link)){
                    $ebook->link = $link;
                }

                $ebook->status = $data['status'];

                if(!$ebook->save()){
                    $json = [
                        'success' => false,
                        'title' => 'Ops!',
                        'message' => $ebook->message()->render()
                    ];
                    echo json_encode($json);
                    return;
                }

                $this->message->success("E-book atualizado com sucesso...")->flash();
                $json = [
                    'success' => true,
                    'title' => 'Sucesso',
                    'message' => 'E-book atualizado com sucesso...',
                    'redirect' => AppFunctions::appUrl('/ebooks/files')
                ];
                echo json_encode($json);
                return;
            }
        }
    }

    public function ebookDeleteFile(?array $data){
        if(Functions::user()->level < 3){
            $json = [
                'success' => false,
                'title' => 'Ops!',
                'message' => 'Você não tem permissão para executar esta ação'
            ];
            echo json_encode($json);
            return;
        }

        if(!empty($data['ebook_id'])){
            $ebook = (new EbookModel())->findById($data['ebook_id']);
            if(empty($ebook)){
                $this->message->error("Você tentnou deletar um e-book que não existe")->flash();
                echo json_encode(["redirect" => AppFunctions::appUrl("/ebooks/files")]);
                return;
            }else{
                if ($ebook->image && file_exists(__DIR__ . "/../../../" . Config::UPLOAD_DIR . "/{$ebook->image}")) {
                    unlink(__DIR__ . "/../../../" . Config::UPLOAD_DIR . "/{$ebook->image}");
                    (new Thumb())->flush($ebook->image);
                }

                $ebook->destroy();

                $this->message->success("O e-book foi deletado com sucesso...")->flash();
                $json = [
                    'success' => true,
                    'title' => 'Sucesso',
                    'message' => 'O e-book foi deletado com sucesso...',
                    'redirect' => AppFunctions::appUrl('/ebooks/files')
                ];
                echo json_encode($json);

                return;
            }
        }
    }

    public function ebookPages(): void
    {
        $data = [
            'page_name' => 'E-book - Páginas',
            'head' => $this->seo->render(
                "E-book - Páginas | CoreCMS - Gerenciador de Conteúdo Web Modular",
                "Você no controle das suas informações",
                AppFunctions::appUrl(),
                AppFunctions::appTheme("/assets/images/appShare.jpg", Config::VIEW_ADMIN),
                false
            )
        ];
        echo $this->view->render("widgets/ebooks/pages", $data);
    }



}