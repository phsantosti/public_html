<?php


namespace Application\Controllers\Application;


use Application\Config\Config;
use Application\Core\Controller;
use Application\Helpers\AppFunctions;
use Application\Helpers\Functions;
use Application\Libraries\Session;
use Application\Models\UserModel;

class LoginController extends Controller
{
    public function __construct()
    {
        parent::__construct(__DIR__ . "/../../../../public_html/themes/" . Config::VIEW_ADMIN . "/");
    }

    public function index(): void
    {
        //var_dump(true);
        $data = [
            'head' => $this->seo->render(
                "CoreCMS - Gerenciador de Conteúdo Web Modular",
                "Você no controle das suas informações",
                AppFunctions::appUrl(),
                AppFunctions::appTheme("/assets/images/appShare.jpg", Config::VIEW_ADMIN),
                false
            )
        ];
        echo $this->view->render("widgets/login/login", $data);
//        echo $this->view->render("widgets/login/login", $data);
    }

    public function login(?array $data): void{
        if(!empty($data)){
            if(!Functions::csrfVerify($data)){
                $json = [
                    "redirect" => false,
                    "success" => false,
                    "title" => "Ops!",
                    "message" => "Erro ao enviar, use o formulário"
                ];
                echo json_encode($json);
                return;
            }

            $data = (object)$data;

            if(empty($data->email)){
                $json = [
                    "redirect" => false,
                    "success" => false,
                    "title" => "Ops!",
                    "message" => $this->message->error("O Campo e-mail não pode ficar em branco")->render()
                ];
                echo json_encode($json);
                return;
            }

            if(empty($data->password)){
                $json = [
                    "redirect" => false,
                    "success" => false,
                    "title" => "Ops!",
                    "message" => $this->message->error("O campo senha não pode ficar em branco")->render()
                ];
                echo json_encode($json);
                return;
            }

            if(!Functions::isEmail($data->email)){
                $json = [
                    "redirect" => false,
                    "success" => false,
                    "title" => "Ops!",
                    "message" => $this->message->error("O e-mail informado não é válido")->render()
                ];
                echo json_encode($json);
                return;
            }

            if(!Functions::isPassword($data->password)){
                $json = [
                    "redirect" => false,
                    "success" => false,
                    "title" => "Ops!",
                    "message" => $this->message->error("A senha informada não é válida")->render()
                ];
                echo json_encode($json);
                return;
            }

            $user = (new UserModel())->findByEmail($data->email);

            if(empty($user)){
                $json = [
                    "redirect" => false,
                    "success" => false,
                    "title" => "Ops!",
                    "message" => $this->message->error("E-mail não cadastrado")->render()
                ];
                echo json_encode($json);
                return;
            }

            if(!Functions::validateLevelLogin($user->level)){
                $json = [
                    "redirect" => false,
                    "success" => false,
                    "title" => "Ops!",
                    "message" => $this->message->error("Você não tem permissão de acesso.")->render()
                ];
                echo json_encode($json);
                return;
            }

            if(!Functions::passwordVerify($data->password, $user->password)){
                $json = [
                    "redirect" => false,
                    "success" => false,
                    "title" => "Ops!",
                    "message" => "Senha não confere"
                ];
                echo json_encode($json);
                return;
            }else{
                if(Functions::passwordReHash($user->password)){
                    $user->password = Functions::password($data->password);
                    $user->save();
                }
            }

            $session = new Session();

            $session->set('user_logged', $user);

            if($session->has('user_logged')){
                $json = [
                    "redirect" => AppFunctions::appUrl('/dashboard'),
                    "success" => true,
                    "title" => "Sucesso!",
                    "message" => $this->message->success("Olá {$session->user_logged->name}, já vamos redirecionar você")->render()
                ];
                echo json_encode($json);
                return;
            }
        }else{
            $json = [
                "redirect" => false,
                "success" => false,
                "title" => "Ops!",
                "message" => $this->message->error("Informe um e-mail e senha para continuar")->render()
            ];
            echo json_encode($json);
            return;
        }

    }

    public function logout(): void{
        $session = new Session();
        $session->destroy();
        AppFunctions::appRredirect('/');
    }
}