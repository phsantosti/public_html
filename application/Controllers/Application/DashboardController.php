<?php


namespace Application\Controllers\Application;


use Application\Config\Config;
use Application\Core\Controller;
use Application\Helpers\AppFunctions;
use Application\Helpers\Functions;
use Application\Libraries\Session;
use Application\Models\EbookModel;
use Application\Models\FolderModel;
use Application\Models\PageEbookModel;
use Application\Models\PageFolderModel;
use Application\Models\PostModel;
use Application\Models\UserModel;

class DashboardController extends Controller
{
    public function __construct()
    {
        parent::__construct(__DIR__ . '/../../../../public_html/themes/' . Config::VIEW_ADMIN . '/');

        AppFunctions::checkUserLogged();
    }

    public function index(): void
    {
        $data = [
            'ebookCount' => (new EbookModel())->find("status = :status", "status=1")->count(),
            'folderCount' => (new FolderModel())->find("status = :status", "status=1")->count(),
            'postCount' => 0,
            'courseCount' => 0,
            'page_name' => 'Dashboard',
            'head' => $this->seo->render(
                "Dashboard | CoreCMS - Gerenciador de Conteúdo Web Modular",
                "Você no controle das suas informações",
                AppFunctions::appUrl(),
                AppFunctions::appTheme("/assets/images/appShare.jpg", Config::VIEW_ADMIN),
                false
            ),
        ];
        //var_dump($data);
        echo $this->view->render("widgets/dash/home", $data);
    }

}