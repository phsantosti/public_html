<?php


namespace Application\Controllers\Application;


use Application\Config\Config;
use Application\Core\Controller;
use Application\Helpers\AppFunctions;
use Application\Helpers\Functions;
use Application\Models\LevelModel;
use Application\Models\UserModel;

class LevelController extends Controller
{
    public function __construct()
    {
        parent::__construct(__DIR__ . "/../../../../public_html/themes/" . Config::VIEW_ADMIN . "/");
        AppFunctions::checkUserLogged();
    }

    public function index(): void
    {
        $data = [
            'page_name' => 'Níveis de Acesso',
            'head' => $this->seo->render(
                "Níveis de Acesso | CoreCMS - Gerenciador de Conteúdo Web Modular",
                "Você no controle das suas informações",
                AppFunctions::appUrl(),
                AppFunctions::appTheme("/assets/images/appShare.jpg", Config::VIEW_ADMIN),
                false
            ),
        ];
        echo $this->view->render("widgets/levels/home", $data);
    }

    public function create(?array $data): void
    {
        if(Functions::user()->level < 3){
            $json = [
                'success' => false,
                'title' => 'Ops!',
                'message' => 'Você não tem permissão para executar esta ação'
            ];
            echo json_encode($json);
            return;
        }

        if(!empty($data)){
            if(empty($data['name'])){
                $json = [
                    'success' => false,
                    'title' => 'Ops!',
                    'message' => 'O campo Nome é obrigatório'
                ];
                echo json_encode($json);
                return;
            }

            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $data = (object)$data;

            $levelModel = new LevelModel();
            $levelModel->bootstrap($data->name, $data->status);
            if(!$levelModel->save()){
                $json = [
                    'success' => false,
                    'title' => 'Ops!',
                    'message' => $levelModel->message()->render()
                ];
                echo json_encode($json);
                return;
            }

            $this->message->success("Nível de Acesso cadastrado com sucesso...")->flash();
            $json = [
                'success' => true,
                'title' => 'Sucesso',
                'message' => 'Nível de Acesso cadastrado com sucesso...',
                'redirect' => AppFunctions::appUrl("/levels")
            ];
            echo json_encode($json);
            return;
        }
    }

    public function read(?array $data)
    {
        if(!empty($data['level_id'])){
            $levelId = filter_var($data['level_id'], FILTER_VALIDATE_INT);
            $level = (new LevelModel())->findById($levelId);
            if(!empty($level)){
                $json = [
                    'success' => true,
                    'title' => 'Sucesso',
                    'message' => 'Usuário encontrado...',
                    'level' => [
                        'id' => $level->id,
                        'name' => $level->name,
                        'status' => $level->status,
                        'created_at' => $level->created_at,
                        'updated_at' => $level->updated_at
                    ],
                    'action' => AppFunctions::appUrl("/levels/level/{$level->id}")
                ];
                echo json_encode($json);
                return;
            }
        }else{
            $levels = (new LevelModel())->find()->fetch(true);
            $arrayLevels = [];
            if(!empty($levels)){
                foreach ($levels as $level){
                    $l = new \stdClass();
                    $l->id = $level->id;
                    $l->name = $level->name;
                    $l->status = ($level->status == 1 ? "Ativo" : "Inativo");
                    $l->created_at = Functions::dateFormatBraazil($level->created_at);
                    $l->updated_at = Functions::dateFormatBraazil($level->updated_at);
                    $l->options =
                        "<button data-toggle='tooltip' data-placement='left' data-original-title='Editar Nível de Acesso' title type='button' class='btn btn-primary text-white font-weight-bold' id='".$level->id."' onclick='editLevel(this.id);'><i class='icon-note'></i></button> ".
                        "<button data-toggle='tooltip' data-placement='left' data-original-title='Deletar Nível de Acesso' title type='button' class='btn btn-danger text-white font-weight-bold' id='".$level->id."' onclick='deleteLevel(this.id);'><i class='icon-close'></i> </button> ";
                    array_push($arrayLevels, $l);
                }

                $json['data'] = $arrayLevels;
                echo json_encode($json, JSON_PRETTY_PRINT);
                return;
            }

        }
    }

    public function update(?array $data){
        if(Functions::user()->level < 3){
            $json = [
                'success' => false,
                'title' => 'Ops!',
                'message' => 'Você não tem permissão para executar esta ação'
            ];
            echo json_encode($json);
            return;
        }

        if(!empty($data['level_id'])){
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $data = (object)$data;
            $level = (new LevelModel())->findById($data->level_id);

            if(empty($level)){
                $this->message->error("Você tentou gerenciar um nível de acesso que não existe")->flash();
                echo json_encode(["redirect" => AppFunctions::appUrl("/levels")]);
                return;
            }

            $level->id = $data->level_id;
            $level->name = $data->name;
            $level->status = $data->status;

            if(!$level->save()){
                $json['message'] = $level->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Nível de Acesso atualizado com sucesso...")->flash();
            $json['success'] = true;
            $json['redirect'] = AppFunctions::appUrl('/levels');
            echo json_encode($json);
            return;
        }else{
            $json = [
                'success' => false,
                'title' => 'Ops!',
                'message' => 'O nível de acesso que está tentando atualizar não existe mais, atualize a página para recarregar a lista de níveis de acesso cadastrados'
            ];
            echo json_encode($json);
            return;
        }
    }

    public function delete(?array $data){
        if(Functions::user()->level < 3){
            $json = [
                'success' => false,
                'title' => 'Ops!',
                'message' => 'Você não tem permissão para executar esta ação'
            ];
            echo json_encode($json);
            return;
        }

        if (!empty($data["level_id"])){
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $level = (new LevelModel())->findById($data['level_id']);

            if (!$level) {
                $this->message->error("Você tentnou deletar um nível de acesso que não existe")->flash();
                echo json_encode(["redirect" => AppFunctions::appUrl("/levels")]);
                return;
            }

            $level->destroy();

            $this->message->success("O nível de acesso foi excluído com sucesso...")->flash();
            echo json_encode(["redirect" => AppFunctions::appUrl("/levels")]);

            return;
        }else{
            $json = [
                'success' => false,
                'title' => 'Ops!',
                'message' => 'O nível de acesso que está tentando deletar não existe mais, atualize a página para recarregar a lista de níveis de acesso cadastrados'
            ];
            echo json_encode($json);
            return;
        }
    }
}