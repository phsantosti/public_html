<?php


namespace Application\Controllers\Application;


use Application\Config\Config;
use Application\Core\Controller;
use Application\Helpers\AppFunctions;
use Application\Helpers\Functions;
use Application\Libraries\Upload;
use Application\Models\LevelModel;
use Application\Models\UserModel;
use Application\Libraries\Thumb;

class UserController extends Controller
{
    public function __construct()
    {
        parent::__construct(__DIR__ . "/../../../../public_html/themes/" . Config::VIEW_ADMIN . "/");
        AppFunctions::checkUserLogged();
    }

    public function index(): void
    {
        $data = [
            'page_name' => 'Usuários',
            'head' => $this->seo->render(
                "Usuários | CoreCMS - Gerenciador de Conteúdo Web Modular",
                "Você no controle das suas informações",
                AppFunctions::appUrl(),
                AppFunctions::appTheme("/assets/images/appShare.jpg", Config::VIEW_ADMIN),
                false
            ),
        ];
        echo $this->view->render("widgets/users/home", $data);
    }

    public function create(?array $data): void
    {
        if(Functions::user()->level < 3){
            $json = [
                'success' => false,
                'title' => 'Ops!',
                'message' => 'Você não tem permissão para executar esta ação'
            ];
            echo json_encode($json);
            return;
        }

        if(!empty($data)){
            if(empty($data['name'])){
                $json = [
                    'success' => false,
                    'title' => 'Ops!',
                    'message' => 'O campo Nome é obrigatório'
                ];
                echo json_encode($json);
                return;
            }

            if(empty($data['lastname'])){
                $json = [
                    'success' => false,
                    'title' => 'Ops!',
                    'message' => 'O campo Sobrenome é obrigatório'
                ];
                echo json_encode($json);
                return;
            }

            if(empty($data['email'])){
                $json = [
                    'success' => false,
                    'title' => 'Ops!',
                    'message' => 'O campo E-mail é obrigatório'
                ];
                echo json_encode($json);
                return;
            }

            if(!Functions::isEmail($data['email'])){
                $json = [
                    'success' => false,
                    'title' => 'Ops!',
                    'message' => 'O e-mail informado não é válido'
                ];
                echo json_encode($json);
                return;
            }

            if(empty($data['password'])){
                $json = [
                    'success' => false,
                    'title' => 'Ops!',
                    'message' => 'A senha informada não é válida'
                ];
                echo json_encode($json);
                return;
            }

            if(!Functions::passwordLimitCheck($data['password'])){
                $json = [
                    'success' => false,
                    'title' => 'Ops!',
                    'message' => 'A senha informada precisa ter entre 8 a 40 caracteres'
                ];
                echo json_encode($json);
                return;
            }

            if(!empty($_FILES['avatar'])){
                $files = $_FILES['avatar'];
                $upload = new Upload();
                $image = $upload->image($files, Functions::stringSlug($data['name']), 600);
                if(!$image){
                    $json = [
                        'success' => false,
                        'title' => 'Ops!',
                        'message' => $upload->message()->render()
                    ];
                    echo json_encode($json);
                    return;
                }
            }

            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $data = (object)$data;

            $userModel = new UserModel();
            $userModel->bootstrap($data->name,$data->lastname,$data->email,Functions::password($data->password),($image ?? "images/avatar.jpg"),$data->level,$data->status);
            if(!$userModel->save()){
                $json = [
                    'success' => false,
                    'title' => 'Ops!',
                    'message' => $userModel->message()->render()
                ];
                echo json_encode($json);
                return;
            }

            $this->message->success("Usuário cadastrado com sucesso...")->flash();
            $json = [
                'success' => true,
                'title' => 'Sucesso',
                'message' => 'Cadastro realizado com sucesso',
                'redirect' => AppFunctions::appUrl("/users")
            ];
            echo json_encode($json);
            return;
        }
    }

    public function read(?array $userData)
    {
        if(!empty($userData['user_id'])){
            $userId = filter_var($userData['user_id'], FILTER_VALIDATE_INT);
            $userEdit = (new UserModel())->findById($userId);
            if(!empty($userEdit)){
                $json = [
                    'success' => true,
                    'title' => 'Sucesso',
                    'message' => 'Usuário encontrado...',
                    'user' => [
                        'id' => $userEdit->id,
                        'name' => $userEdit->name,
                        'lastname' => $userEdit->lastname,
                        'email' => $userEdit->email,
                        'avatar' => AppFunctions::image($userEdit->avatar, 600),
                        'level' => $userEdit->level,
                        'status' => $userEdit->status,
                    ],
                    'action' => AppFunctions::appUrl("/users/user/{$userEdit->id}")
                ];
                echo json_encode($json);
                return;
            }
        }else{
            $users = (new UserModel())->find()->fetch(true);
            $arrayUsers = [];
            if(!empty($users)){
                foreach ($users as $user){
                    $u = new \stdClass();
                    $u->id = $user->id;
                    $u->full_name = $user->name . ' ' . $user->lastname;
                    $u->email = $user->email;
                    $u->level = (new LevelModel())->findById($user->level)->name;
                    $u->status = ($user->status == 1 ? "Ativo" : "Inativo");
                    $u->created_at = Functions::dateFormatBraazil($user->created_at);
                    $u->updated_at = Functions::dateFormatBraazil($user->updated_at);
                    $u->options =
                        "<button data-toggle='tooltip' data-placement='left' data-original-title='Editar Usuário' title type='button' class='btn btn-primary btn-edit-user text-white font-weight-bold' id='".$user->id."' onclick='editUser(this.id);'><i class='icon-note'></i></button> ".
                        "<button data-toggle='tooltip' data-placement='left' data-original-title='Deletar Usuário' title='Deletar Usuário' type='button' class='btn btn-danger btn-delete-user text-white font-weight-bold' id='".$user->id."' onclick='deleteUser(this.id);'><i class='icon-close'></i> </button> ".
                        "<button data-toggle='tooltip' data-placement='left' data-original-title='Trocar Senha' title='Trocar Senha' type='button' class='btn btn-warning btn-change-password text-white font-weight-bold' id='".$user->id."' onclick='changePasswordUser(this.id);'><i class='icon-key'></i></button>";

                    array_push($arrayUsers, $u);
                }

                $json['data'] = $arrayUsers;
                echo json_encode($json, JSON_PRETTY_PRINT);
                return;
            }

        }
    }

    public function update(?array $userData){
        if(Functions::user()->level < 3){
            $json = [
                'success' => false,
                'title' => 'Ops!',
                'message' => 'Você não tem permissão para executar esta ação'
            ];
            echo json_encode($json);
            return;
        }

        if(!empty($userData['user_id'])){
            $userData = filter_var_array($userData, FILTER_SANITIZE_STRIPPED);
            $userData = (object)$userData;
            $user = (new UserModel())->findById($userData->user_id);
            if(empty($user)){
                $this->message->error("Você tentou gerenciar um usuário que não existe")->flash();
                echo json_encode(["redirect" => AppFunctions::appUrl("/users")]);
                return;
            }

            if(!empty($_FILES['avatar'])){
                $files = $_FILES['avatar'];
                $upload = new Upload();
                $image = $upload->image($files, md5($userData->name), 600);
                if(!$image){
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }
            }

            $image = ($image ?? $user->avatar);

            $password = (!empty($userData->password) ? Functions::password($userData->password) : $user->password);

            $user->id = $userData->user_id;

            $user->bootstrap($userData->name,$userData->lastname,$userData->email,$password,$image,$userData->level,$userData->status);
            if(!$user->save()){
                $json['message'] = $user->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Usuário atualizado com sucesso...")->flash();
            $json['success'] = true;
            $json['redirect'] = AppFunctions::appUrl('/users');
            echo json_encode($json);
            return;
        }else{
            $json = [
                'success' => false,
                'title' => 'Ops!',
                'message' => 'O usuário que está tentando atualizar não existe mais, atualize a página para recarregar a lista de usuários cadastrados'
            ];
            echo json_encode($json);
            return;
        }
    }

    public function changePassword(?array $userData){
        if(Functions::user()->level < 3){
            $json = [
                'success' => false,
                'title' => 'Ops!',
                'message' => 'Você não tem permissão para executar esta ação'
            ];
            echo json_encode($json);
            return;
        }

        if(!empty($userData['user_id'])){
            if($userData['password'] != $userData['passwordConfirm']){
                $json = [
                    'success' => false,
                    'title' => 'Ops!',
                    'message' => 'A senha digitada não confere com a senha de confirmação, verifique os dados e tente novamente'
                ];
                echo json_encode($json);
                return;
            }

            $userData = filter_var_array($userData, FILTER_SANITIZE_STRIPPED);
            $userData = (object)$userData;
            $user = (new UserModel())->findById($userData->user_id);

            if(empty($user)){
                $this->message->error("Você tentou gerenciar um usuário que não existe")->flash();
                echo json_encode(["redirect" => AppFunctions::appUrl("/users")]);
                return;
            }

            if(!Functions::passwordLimitCheck($userData->password)){
                $json = [
                    'success' => false,
                    'title' => 'Ops!',
                    'message' => 'A senha digitada deve possui entre 8 a 40 caracteres'
                ];
                echo json_encode($json);
                return;
            }

            $user->password = Functions::password($userData->password);

            if(!$user->save()){
                $json['message'] = $user->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Usuário atualizado com sucesso...")->flash();
            $json['success'] = true;
            $json['redirect'] = AppFunctions::appUrl('/users');
            echo json_encode($json);
            return;

        }else{
            $json = [
                'success' => false,
                'title' => 'Ops!',
                'message' => 'O usuário que está tentando atualizar não existe mais, atualize a página para recarregar a lista de usuários cadastrados'
            ];
            echo json_encode($json);
            return;
        }

    }

    public function delete(?array $userData){
        if(Functions::user()->level < 3){
            $json = [
                'success' => false,
                'title' => 'Ops!',
                'message' => 'Você não tem permissão para executar esta ação'
            ];
            echo json_encode($json);
            return;
        }

        if (!empty($userData["user_id"])){
            $userData = filter_var_array($userData, FILTER_SANITIZE_STRIPPED);
            $userDelete = (new UserModel())->findById($userData["user_id"]);

            if (!$userDelete) {
                $this->message->error("Você tentnou deletar um usuário que não existe")->flash();
                echo json_encode(["redirect" => AppFunctions::appUrl("/users")]);
                return;
            }

            if ($userDelete->avatar && file_exists(__DIR__ . "/../../../" . Config::UPLOAD_DIR . "/{$userDelete->avatar}")) {
                unlink(__DIR__ . "/../../../" . Config::UPLOAD_DIR . "/{$userDelete->avatar}");
                (new Thumb())->flush($userDelete->avatar);
            }

            $userDelete->destroy();

            $this->message->success("O usuário foi excluído com sucesso...")->flash();
            echo json_encode(["redirect" => AppFunctions::appUrl("/users")]);

            return;
        }else{
            $json = [
                'success' => false,
                'title' => 'Ops!',
                'message' => 'O usuário que está tentando deletar não existe mais, atualize a página para recarregar a lista de usuários cadastrados'
            ];
            echo json_encode($json);
            return;
        }
    }
}