<?php


namespace Application\Controllers\Application;


use Application\Config\Config;
use Application\Core\Controller;
use Application\Helpers\Functions;
use Application\Models\NotificationModel;

class NotificationController extends Controller
{
    public function __construct()
    {
        parent::__construct(__DIR__ . "/../../../../public_html/themes/" . Config::VIEW_ADMIN . "/");
    }

    public function count()
    {
        $json['count'] = (new NotificationModel())->find("view < 1")->count();
        echo json_encode($json);
    }

    public function list()
    {
        $notifications = (new NotificationModel())->order("view ASC, created_at DESC")->limit(10)->find()->fetch(true);
        if(!$notifications){
            $json["message"] = $this->message->info("No momento não existem notificações por aqui")->render();
            echo json_encode($json);
            return;
        }

        $notificationList = null;

        foreach ($notifications as $notification){
            $notification->view = 1;
            $notification->save();
            $notification->created_at = Functions::dateFormat($notification->created_at);
            $notificationList[] = $notification->data();
        }
        echo json_encode(['notifications' => $notificationList]);
    }

    public function index(): void
    {

    }
}