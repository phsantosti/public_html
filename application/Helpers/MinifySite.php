<?php
use Application\Helpers\Functions;
use MatthiasMullie\Minify\CSS;
use MatthiasMullie\Minify\JS;
use Application\Config\Config;

if(strpos(Functions::url(), "localhost")){
    $minCSS = new CSS();
    $minCSS->add(__DIR__ . '/../../shared/styles/styles.css');
    $minCSS->add(__DIR__ . '/../../shared/styles/boot.css');

    $cssDir = scandir(__DIR__ . '/../../themes/' . Config::VIEW_THEME . '/assets/css');

    foreach ($cssDir as $css){
        $cssFile = __DIR__ . '/../../themes/' . Config::VIEW_THEME . '/assets/css/' . $css;

        if(is_file($cssFile) && pathinfo($cssFile)['extension'] == 'css'){
            $minCSS->add($cssFile);
        }
    }

    $minCSS->minify(__DIR__ . '/../../themes/' . Config::VIEW_THEME . '/assets/style.min.css');

    $minJS = new JS();
    $minJS->add(__DIR__ . "/../../shared/scripts/jquery.min.js");
    $minJS->add(__DIR__ . "/../../shared/scripts/jquery.form.js");
    $minJS->add(__DIR__ . "/../../shared/scripts/jquery-ui.js");
    $minJS->add(__DIR__ . "/../../shared/scripts/tracker.js");

    $jsDir = scandir(__DIR__ . "/../../themes/" . Config::VIEW_THEME . "/assets/js");

    foreach ($jsDir as $js) {
        $jsFile = __DIR__ . "/../../themes/" . Config::VIEW_THEME . "/assets/js/{$js}";

        if (is_file($jsFile) && pathinfo($jsFile)['extension'] == "js") {
            $minJS->add($jsFile);
        }
    }

    $minJS->minify(__DIR__ . "/../../themes/" . Config::VIEW_THEME . "/assets/script.min.js");
}
