<?php


namespace Application\Helpers;


use Application\Config\Config;
use Application\Libraries\Message;
use Application\Libraries\Session;
use Application\Libraries\Thumb;
use Application\Models\LevelModel;
use Application\Models\PermissionModel;
use Application\Models\UserModel;

class AppFunctions
{
    public const APP_URL_BASE = 'https://sistema.pedrohsantos.com.br';
    public const APP_URL_TEST = 'https://www.localhost/public_html/sistema';

    public static function appUrl(string $path = null): string{
        if(strpos($_SERVER['HTTP_HOST'], "localhost")){
            if($path){
                return self::APP_URL_TEST . "/" . ($path[0] == "/" ? mb_substr($path, 1) : $path);
            }

            return self::APP_URL_TEST;
        }

        if($path){
            return self::APP_URL_BASE . "/" . ($path[0] == "/" ? mb_substr($path, 1) : $path);
        }

        return self::APP_URL_BASE;

    }

    public static function appUrlBack(): string{
        return ($_SERVER['HTTP_REFERER'] ?? self::appUrl());
    }

    public static function appTheme(string $path = null, string $theme = Config::VIEW_THEME): string
    {
        if (strpos($_SERVER['HTTP_HOST'], "localhost")) {
            if ($path) {
                return self::APP_URL_TEST . "/themes/{$theme}/" . ($path[0] == "/" ? mb_substr($path, 1) : $path);
            }

            return self::APP_URL_TEST . "/themes/{$theme}";
        }

        if ($path) {
            return self::APP_URL_BASE . "/themes/{$theme}/" . ($path[0] == "/" ? mb_substr($path, 1) : $path);
        }

        return self::APP_URL_BASE . "/themes/{$theme}";
    }

    public static function appImage(?string $image, int $width, int $height = null): ?string
    {
        if($image){
            return self::appUrl() . '/' .(new Thumb())->make($image,$width, $height);
        }
        return null;
    }

    public static function appRredirect(string $url): void
    {
        header("HTTP/1.1 302 Redirect");
        if (filter_var($url, FILTER_VALIDATE_URL)) {
            header("Location: {$url}");
            exit;
        }

        if (filter_input(INPUT_GET, "route", FILTER_DEFAULT) != $url)
        {
            $location = self::appUrl($url);
            header("Location: {$location}");
            exit;
        }
    }

    public static function checkUserLogged(){
        $session = new Session();
        if(empty($session->has('user_logged'))){
            self::appRredirect('/');
        }
    }

    public static function user(): ?UserModel{
        $session = new Session();
        if(!empty($session->has('user_logged'))){
            return $session->user_logged;
        }
        return null;
    }

    /**
     * @param string|null $image
     * @param int $width
     * @param int|null $height
     * @return string|null
     * @throws \Exception
     */
    public static function image(?string $image, int $width, int $height = null): ?string
    {
        if($image){
            $strImage = Functions::url() . '/' .(new Thumb())->make($image,$width, $height);
            return str_replace("..", "", $strImage);
        }
        return null;
    }

    public static function levels(): ?array {
        $objectLevels = [];
        $levels = (new LevelModel())->find("status = :status", "status=1")->fetch(true);
        if(!empty($levels)){
            foreach ($levels as $level){
                $data = $level->data();
                array_push($objectLevels, $data);
            }
            return $objectLevels;
        }
        return $objectLevels;
    }
}
