<?php


namespace Application\Helpers;


class EbookFunctions
{
    public const EBOOK_URL_BASE = 'https://ebooks.plenumbrasil.com.br';
    public const EBOOK_URL_TEST = 'https://www.localhost/phpframework/ebooks';

    public static function ebookUrl(string $path = null): string{
        if(strpos($_SERVER['HTTP_HOST'], "localhost")){
            if($path){
                return self::EBOOK_URL_TEST . "/" . ($path[0] == "/" ? mb_substr($path, 1) : $path);
            }

            return self::EBOOK_URL_TEST;
        }

        if($path){
            return self::EBOOK_URL_BASE . "/" . ($path[0] == "/" ? mb_substr($path, 1) : $path);
        }

        return self::EBOOK_URL_BASE;

    }

    public static function ebookUrlBack(): string{
        return ($_SERVER['HTTP_REFERER'] ?? self::ebookUrl());
    }
}
