<?php


namespace Application\Helpers;


use Application\Config\Config;
use Application\Libraries\Thumb;
use Application\Models\UserModel;
use Application\Libraries\Session;
use DateTime;

class Functions
{
    public static function isEmail(string $email): bool
    {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    public static function isPassword(string $password): bool
    {
        if (password_get_info($password)['algo'] || (mb_strlen($password) >= Config::PASSWORD_MIN_LEN && mb_strlen($password) <= Config::PASSWORD_MAX_LEN)) {
            return true;
        }

        return false;
    }

    public static function stringSlug(string $string): string
    {
        $string = filter_var(mb_strtolower($string), FILTER_SANITIZE_STRIPPED);
        $formats = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜüÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿRr"!@#$%&*()_-+={[}]/?;:.,\\\'<>°ºª';
        $replace = 'aaaaaaaceeeeiiiidnoooooouuuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr                                 ';

        $slug = str_replace(["-----", "----", "---", "--"], "-",
            str_replace(" ", "-",
                trim(strtr(utf8_decode($string), utf8_decode($formats), $replace))
            )
        );
        return $slug;
    }

    public static function stringStudlyCase(string $string): string
    {
        $string = self::stringSlug($string);
        $studlyCase = str_replace(" ", "",
            mb_convert_case(str_replace("-", " ", $string), MB_CASE_TITLE)
        );

        return $studlyCase;
    }

    public static function stringCamelCase(string $string): string
    {
        return lcfirst(self::stringStudlyCase($string));
    }

    public static function stringTitle(string $string): string
    {
        return mb_convert_case(filter_var($string, FILTER_SANITIZE_SPECIAL_CHARS), MB_CASE_TITLE);
    }

    public static function stringTextArea(string $text): string
    {
        $text = filter_var($text, FILTER_SANITIZE_STRIPPED);
        $arrayReplace = ["&#10;", "&#10;&#10;", "&#10;&#10;&#10;", "&#10;&#10;&#10;&#10;", "&#10;&#10;&#10;&#10;&#10;"];
        return "<p>" . str_replace($arrayReplace, "</p><p>", $text) . "</p>";
    }

    public static function stringLimitWords(string $string, int $limit, string $pointer = '...'): string
    {
        $string = trim(filter_var($string, FILTER_SANITIZE_SPECIAL_CHARS));
        $arrWords = explode(" ", $string);
        $numWords = count($arrWords);

        if ($numWords < $limit) {
            return $string;
        }

        $words = implode(" ", array_slice($arrWords, 0, $limit));
        return "{$words}{$pointer}";
    }

    public static function stringLimitChars(string $string, int $limit, string $pointer = "..."): string
    {
        $string = trim(filter_var($string, FILTER_SANITIZE_SPECIAL_CHARS));
        if (mb_strlen($string) <= $limit) {
            return $string;
        }

        $chars = mb_substr($string, 0, mb_strrpos(mb_substr($string, 0, $limit), " "));
        return "{$chars}{$pointer}";
    }

    public static function stringPrice(?string $price): string
    {
        return number_format((!empty($price) ? $price : 0), 2, ",", ".");
    }

    public static function stringSearch(?string $search): string
    {
        if (!$search) {
            return "all";
        }

        $search = preg_replace("/[^a-z0-9A-Z\@\ ]/", "", $search);
        return (!empty($search) ? $search : "all");
    }

    public static function url(string $path = null): string
    {
        if (strpos($_SERVER['HTTP_HOST'], "localhost")) {
            if ($path) {
                return Config::URL_TEST . "/" . ($path[0] == "/" ? mb_substr($path, 1) : $path);
            }
            return Config::URL_TEST;
        }

        if ($path) {
            return Config::URL_BASE . "/" . ($path[0] == "/" ? mb_substr($path, 1) : $path);
        }

        return Config::URL_BASE;
    }

    public static function urlBack(): string
    {
        return ($_SERVER['HTTP_REFERER'] ?? self::url());
    }

    public static function redirect(string $url): void
    {
        header("HTTP/1.1 302 Redirect");
        if (filter_var($url, FILTER_VALIDATE_URL)) {
            header("Location: {$url}");
            exit;
        }

        if (filter_input(INPUT_GET, "route", FILTER_DEFAULT) != $url) {
            $location = self::url($url);
            header("Location: {$location}");
            exit;
        }
    }

    public static function user(): ?UserModel
    {
        $session = new Session();
        if (!$session->has('user_logged')) {
            return null;
        }
        return (new UserModel())->findById($session->user_logged->id);
    }

    public static function session(): Session
    {
        return new Session();
    }

    public static function theme(string $path = null, string $theme = Config::VIEW_THEME): string
    {
        if (strpos($_SERVER['HTTP_HOST'], "localhost")) {
            if ($path) {
                return Config::URL_TEST . "/themes/{$theme}/" . ($path[0] == "/" ? mb_substr($path, 1) : $path);
            }

            return Config::URL_TEST . "/themes/{$theme}";
        }

        if ($path) {
            return Config::URL_BASE . "/themes/{$theme}/" . ($path[0] == "/" ? mb_substr($path, 1) : $path);
        }

        return Config::URL_BASE . "/themes/{$theme}";
    }

    public static function image(?string $image, int $width, int $height = null): ?string
    {
        if ($image) {
            if (strpos($_SERVER['HTTP_HOST'], "localhost")) {
                return (new Thumb())->make($image, $width, $height);
            }
            return Functions::url() . '/' . (new Thumb())->make($image, $width, $height);

        }
        return null;
    }

    /**
     * @param string|null $date
     * @param string $format
     * @return string
     * @throws \Exception
     */
    public static function dateFormat(?string $date, string $format = "d/m/Y H\hi"): string
    {
        $date = (empty($date) ? "now" : $date);
        return (new DateTime($date))->format($format);
    }


    /**
     * @param string $date
     * @return string
     * @throws \Exception
     */
    public static function dateFormatBraazil(?string $date): string
    {
        $date = (empty($date) ? "now" : $date);
        return (new DateTime($date))->format(Config::DATE_BR);
    }

    /**
     * @param string $date
     * @return string
     * @throws \Exception
     */
    public static function dateFormatApplication(?string $date): string
    {
        $date = (empty($date) ? "now" : $date);
        return (new DateTime($date))->format(Config::DATE_APP);
    }

    /**
     * @param string|null $date
     * @return string|null
     */
    public static function dateFormatBack(?string $date): ?string
    {
        if (!$date) {
            return null;
        }

        if (strpos($date, " ")) {
            $date = explode(" ", $date);
            return implode("-", array_reverse(explode("/", $date[0]))) . " " . $date[1];
        }

        return implode("-", array_reverse(explode("/", $date)));
    }

    /**
     * ####################
     * ###   PASSWORD   ###
     * ####################
     */

    /**
     * @param string $password
     * @return string
     */
    public static function password(string $password): string
    {
        if (!empty(password_get_info($password)['algo'])) {
            return $password;
        }

        return password_hash($password, Config::PASSWORD_ALGO, Config::PASSWORD_OPTION);
    }

    /**
     * @param string $password
     * @param string $hash
     * @return bool
     */
    public static function passwordVerify(string $password, string $hash): bool
    {
        return password_verify($password, $hash);
    }

    /**
     * @param string $hash
     * @return bool
     */
    public static function passwordReHash(string $hash): bool
    {
        return password_needs_rehash($hash, Config::PASSWORD_ALGO, Config::PASSWORD_OPTION);
    }

    public static function passwordLimitCheck(string $password): bool{
        if(strlen($password) < Config::PASSWORD_MIN_LEN || strlen($password) > Config::PASSWORD_MAX_LEN){
            return false;
        }
        return true;
    }

    /**
     * ###################
     * ###   REQUEST   ###
     * ###################
     */

    /**
     * @return string
     */
    public static function csrfInput(): string
    {
        $session = new Session();
        $session->csrf();
        return "<input type='hidden' name='csrf' value='" . ($session->csrf_token ?? "") . "'/>";
    }

    /**
     * @param $request
     * @return bool
     */
    public static function csrfVerify($request): bool
    {
        $session = new Session();
        if (empty($session->csrf_token) || empty($request['csrf']) || $request['csrf'] != $session->csrf_token) {
            return false;
        }
        return true;
    }

    /**
     * @return null|string
     */
    public static function flash(): ?string
    {
        $session = new Session();
        if ($flash = $session->flash()) {
            return $flash;
        }
        return null;
    }

    /**
     * @param string $key
     * @param int $limit
     * @param int $seconds
     * @return bool
     */
    public static function request_limit(string $key, int $limit = 5, int $seconds = 60): bool
    {
        $session = new Session();
        if ($session->has($key) && $session->$key->time >= time() && $session->$key->requests < $limit) {
            $session->set($key, [
                "time" => time() + $seconds,
                "requests" => $session->$key->requests + 1
            ]);
            return false;
        }

        if ($session->has($key) && $session->$key->time >= time() && $session->$key->requests >= $limit) {
            return true;
        }

        $session->set($key, [
            "time" => time() + $seconds,
            "requests" => 1
        ]);

        return false;
    }

    /**
     * @param string $field
     * @param string $value
     * @return bool
     */
    public static function request_repeat(string $field, string $value): bool
    {
        $session = new Session();
        if ($session->has($field) && $session->$field == $value) {
            return true;
        }

        $session->set($field, $value);
        return false;
    }

    public static function validateLevelLogin(int $level): bool
    {
        if (empty($level)) {
            return false;
        } else {
            if ($level < 3) {
                return false;
            } else {
                return true;
            }
        }
    }
}