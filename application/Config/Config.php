<?php


namespace Application\Config;


class Config{
    /**
     * DATABASE CONFIG
     */
    public const DATABASE_HOST = "162.254.204.59";
    public const DATABASE_USER = "pedrohs1_root";
    public const DATABASE_PASSWORD = "Pedro@1524";
    public const DATABASE_NAME = "pedrohs1_phpframework";

    /**
     * URL CONFIG
     */
    public const URL_BASE = 'https://www.pedrohsantos.com.br';
    public const URL_TEST = 'https://www.localhost/public_html';

    /**
     * SITE CONFIG
     */
    public const SITE_NAME = 'Instituto Plenum Brasil';
    public const SITE_TITLE = 'Cursos Online e Presenciais para capacitação na Gestão Pública';
    public const SITE_DESCRIPTION = 'Fortalecendo a Gestão Pública no País';
    public const SITE_LANGUAGE = 'pt_BR';
    public const SITE_DOMAIN = 'plenumbrasil.com.br';

    /**
     * SITE ADDRESS CONFIG
     */
    public const SITE_ADDRESS_STREET = 'Av. do Contorno';
    public const SITE_ADDRESS_NUMBER = '6321';
    public const SITE_ADDRESS_COMPLEMENT = '9º andar';
    public const SITE_ADDRESS_DISTRICT = 'Funcionários';
    public const SITE_ADDRESS_CITY = 'Belo Horizonte';
    public const SITE_ADDRESS_STATE = 'MG';
    public const SITE_ADDRESS_ZIPCODE = '30110-039';

    /**
     * SOCIAL CONFIG
     */
    public const SOCIAL_TWITTER_CREATOR = '@creator';
    public const SOCIAL_TWITTER_PUBLISHER = '@creator';

    public const SOCIAL_FACEBOOK_APP = '1057810364607212';
    public const SOCIAL_FACEBOOK_PAGE = 'InstitutoPlenumBrasil';
    public const SOCIAL_FACEBOOK_AUTHOR = 'phsdevweb';

    public const SOCIAL_GOOGLE_PAGE = '55555555555';
    public const SOCIAL_GOOGLE_AUTHOR = '55555555555';

    public const SOCIAL_INSTAGRAM_PAGE = 'plenumbrasil';

    public const SOCIAL_YOUTUBE_PAGE = 'UCkSJ9k2BL5EdNKVQ4QIK0hA';

    /**
     * DATES CONFIG
     */
    public const DATE_BR = 'd/m/Y H:i:s';
    public const DATE_APP = 'Y-m-d H:i:s';

    /**
     * PASSWORD CONFIG
     */
    public const PASSWORD_MIN_LEN = 8;
    public const PASSWORD_MAX_LEN = 40;
    public const PASSWORD_ALGO = PASSWORD_DEFAULT;
    public const PASSWORD_OPTION = ['cost' => 10];

    /**
     * VIEW CONFIG
     */
    public const VIEW_PATH = __DIR__ . "/../../shared/views";
    public const VIEW_EXTENSION = 'php';
    public const VIEW_THEME = 'plenumbrasil';
    public const VIEW_APP = 'corecms';
    public const VIEW_ADMIN = 'coreui';

    /**
     * UPLOAD CONFIG
     */
    public const UPLOAD_BASE_PROJECT_DIR = "..";
    public const UPLOAD_DIR = 'storage';
    public const UPLOAD_IMAGE_DIR = 'images';
    public const UPLOAD_FILE_DIR = 'files';
    public const UPLOAD_MEDIA_DIR = 'medias';

    /**
     * IMAGES CONFIG
     */
    public const IMAGE_CACHE = self::UPLOAD_BASE_PROJECT_DIR . '/' . self::UPLOAD_DIR . '/' . self::UPLOAD_IMAGE_DIR . '/cache';
    public const IMAGE_SIZE = 2000;
    public const IMAGE_QUALITY = ['jpg' => 75, 'png' => 5];

    /**
     * MAIL CONFIG
     */
    public const MAIL_HOST = 'smtp.sendgrid.net';
    public const MAIL_PORT = '587';
    public const MAIL_USER = 'apikey';
    public const MAIL_PASS = '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';
    public const MAIL_SENDER = ['name' => 'Instituto Plenum Brasil', 'address' => 'contato@plenumbrasil.com'];
    public const MAIL_SUPPORT = 'suporte@plenumbrasil.com';
    public const MAIL_OPTION_LANG = 'br';
    public const MAIL_OPTION_HTML = true;
    public const MAIL_OPTION_AUTH = true;
    public const MAIL_OPTION_SECURE = 'tls';
    public const MAIL_OPTION_CHARSET = 'utf-8';

    /**
     * PAGAR.ME CONFIG
     */
    public const PAGARME_MODE = 'test';
    public const PAGARME_LIVE = 'ak_live_*****';
    public const PAGARME_TEST = 'ak_test_*****';
    public const PAGARME_BACK = self::URL_BASE . '/pay/callback';








































}