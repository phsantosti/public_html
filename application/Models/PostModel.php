<?php


namespace Application\Models;


use Application\Core\Model;

class PostModel extends Model
{
    public function __construct()
    {
        parent::__construct('posts', [''], ['']);
    }
}