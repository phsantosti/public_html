<?php


namespace Application\Models;


use Application\Core\Model;

class FolderModel extends Model
{
    public function __construct()
    {
        parent::__construct('folders', ['id'], ['title', 'description', 'image', 'link', 'downloads', 'status']);
    }

    public function bootstrap(string $title, string $description, string $image, string $link, int $downloads, bool $status): FolderModel
    {
        $this->title = $title;
        $this->description = $description;
        $this->image = $image;
        $this->link = $link;
        $this->downloads = $downloads;
        $this->status = $status;
        return $this;
    }
}