<?php


namespace Application\Models;


use Application\Core\Model;

class UserModel extends Model
{
    public function __construct()
    {
        parent::__construct('users', ['id'], ['name', 'lastname', 'email', 'password', 'avatar', 'level', 'status']);
    }

    public function bootstrap(string $name, string $lastname, string $email, string $password, string $avatar, int $level, int $status): UserModel
    {
        $this->name = $name;
        $this->lastname = $lastname;
        $this->email = $email;
        $this->password = $password;
        $this->avatar = $avatar;
        $this->level = $level;
        $this->status = $status;
        return $this;
    }

    public function findByEmail(string $email): ?UserModel
    {
        $find = $this->find("email = :email", "email={$email}");
        return $find->fetch();
    }

    public function fullName():string{
        return "{$this->name} {$this->lastname}";
    }
}