<?php


namespace Application\Models;


use Application\Core\Model;

class NotificationModel extends Model
{
    public function __construct()
    {
        parent::__construct('notifications', ['id'], ['image', 'title', 'link']);
    }

    public function bootstrap(string $image, string $title, string $link): NotificationModel
    {
        $this->image = $image;
        $this->title = $title;
        $this->link = $link;
        return $this;
    }
}