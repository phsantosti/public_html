<?php


namespace Application\Models;


use Application\Core\Model;

class PermissionModel extends Model
{
    public function __construct()
    {
        parent::__construct('permissions', ['id'], ['level', 'controller', 'method', 'status']);
    }

    public function bootstrap(int $level, string $controller, string $method, bool $status): PermissionModel
    {
        $this->level = $level;
        $this->controller = $controller;
        $this->method = $method;
        $this->status = $status;
        return $this;
    }
}