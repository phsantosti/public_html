<?php


namespace Application\Models;


use Application\Core\Model;

class LevelModel extends Model
{
    public function __construct()
    {
        parent::__construct('levels', ['id'], ['name', 'status']);
    }

    public function bootstrap(string $name, bool $status): LevelModel
    {
        $this->name = $name;
        $this->status = $status;
        return $this;
    }
}