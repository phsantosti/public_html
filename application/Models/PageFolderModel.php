<?php


namespace Application\Models;


use Application\Core\Model;

class PageFolderModel extends Model
{
    public function __construct()
    {
        parent::__construct('pages_folders', ['id'], ['title', 'description', 'cover', 'file']);
    }

    public function bootstrap(string $title, string $description, string $cover, int $file, bool $status): PageFolderModel
    {
        $this->title = $title;
        $this->description = $description;
        $this->cover = $cover;
        $this->file = $file;
        $this->status = $file;
        return $this;
    }
}