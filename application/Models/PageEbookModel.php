<?php


namespace Application\Models;


use Application\Core\Model;

class PageEbookModel extends Model
{
    public function __construct()
    {
        parent::__construct('pages_ebooks', ['id'], ['title', 'subtitle', 'feature_one', 'feature_two','feature_three', 'feature_four', 'author_name', 'author_words', 'author_image', 'file', 'status']);
    }

    public function bootstrap(string $title, string $subtitle, string $feature_one, string $feature_two, string $feature_three, string $feature_four, string $author_name, string $author_words, string $author_image, int $file, bool $status): PageEbookModel{
        $this->title = $title;
        $this->subtitle = $subtitle;
        $this->feature_one = $feature_one;
        $this->feature_two = $feature_two;
        $this->feature_three = $feature_three;
        $this->feature_four = $feature_four;
        $this->author_name = $author_name;
        $this->author_words = $author_words;
        $this->author_image = $author_image;
        $this->file = $file;
        $this->status = $status;
        return $this;

    }
}