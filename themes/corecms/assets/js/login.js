$(function () {
    $("form").submit(function (e) {
        e.preventDefault();

        var form = $(this);
        var load = $(".ajax_load");

        load.fadeIn(200).css("display", "flex");

        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: form.serialize(),
            dataType: "json",
            success: function (response) {
                //Response Message
                if (response.message) {
                    $(".ajax_response").html(response.message).effect("bounce");
                }

                setTimeout(function () {
                    //redirect
                    if (response.redirect) {
                        window.location.href = response.redirect;
                    } else {
                        load.fadeOut(200);
                    }
                }, 3500);
            },
            error: function (response) {
                load.fadeOut(200);
            }
        });
    });
});