<?php

use Application\Helpers\Functions;
use Application\Config\Config;
use Application\Helpers\AppFunctions;
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <?= $head; ?>
    <link rel="stylesheet" href="<?= Functions::url("/shared/styles/boot.css"); ?>"/>
    <link rel="stylesheet" href="<?= Functions::url("/shared/styles/styles.css"); ?>"/>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= Functions::url('/vendor/select2/select2/dist/css/select2.min.css');?>">
    <link rel="stylesheet" href="<?= Functions::url('/shared/styles/izimodal/css/iziModal.min.css');?>">
    <link rel="stylesheet" href="<?= Functions::theme("/assets/css/style.css", Config::VIEW_ADMIN); ?>"/>
    <link rel="icon" type="image/png" href="<?= Functions::theme("/assets/images/favicon.png", Config::VIEW_ADMIN); ?>"/>
</head>
<body>

<div class="ajax_load" style="z-index: 999;">
    <div class="ajax_load_box">
        <div class="ajax_load_box_circle"></div>
        <p class="ajax_load_box_title">Aguarde, carregando...</p>
    </div>
</div>

<div class="ajax_response"><?= Functions::flash(); ?></div>

<div class="dash">
    <aside class="dash_sidebar">
        <article class="dash_sidebar_user">
            <?php
            $photo = Functions::user()->avatar;
            $userPhoto = ($photo ? AppFunctions::image($photo, 300, 300) : Functions::theme("/assets/images/avatar.jpg", Config::VIEW_ADMIN));
            ?>
            <div><img class="dash_sidebar_user_thumb" src="<?= $userPhoto; ?>" alt="" title=""/></div>
            <h3 class="dash_sidebar_user_name">
                <a href="<?= Functions::url("/admin/users/user/" . Functions::user()->id); ?>"><?= Functions::user()->fullName(); ?></a>
            </h3>
        </article>

        <ul class="dash_sidebar_nav">
            <?php
            $app = "";
            $nav = function ($icon, $href, $title) use ($app) {
                $active = (explode("/", $app)[0] == explode("/", $href)[0] ? "active" : null);
                $url = AppFunctions::appUrl("/{$href}");
                return "<li class=\"dash_sidebar_nav_li {$active}\"><a class=\"icon-{$icon}\" href=\"{$url}\" title=\"{$title}\">{$title}</a></li>";
            };

            echo $nav("home", "dashboard", "Dashboard");
            echo $nav("bookmark", "ebooks", "E-books");
            echo $nav("bookmark", "folders", "Folders");
            echo $nav("bookmark", "blog", "Blog");
            echo $nav("user", "users", "Usuários");
            echo "<li class=\"dash_sidebar_nav_li\"><a class=\"icon-link\" href=\"" . Functions::url() . " \" target=\"_blank\">Ver site</a></li>";

            echo $nav("sign-out on_mobile", "logout", "Sair");
            ?>
        </ul>
    </aside>
    <section class="dash_content">
        <div class="dash_userbar">
            <div class="dash_userbar_box">
                <div class="dash_content_box">
                    <h1 class="icon-cog transition"><a href="<?= AppFunctions::appUrl("/dashboard"); ?>">Core<b>CMS</b></a></h1>
                    <div class="dash_userbar_box_bar">
                        <span class="notification_center_open icon-bell" data-count="<?= AppFunctions::appUrl("/notifications/count"); ?>" data-notify="<?= AppFunctions::appUrl("/notifications/list"); ?>">0</span>
                        <span class="no_mobile icon-clock-o"><?= date("d/m/Y H:i:s"); ?></span>
                        <a class="no_mobile icon-sign-out" title="Sair" href="<?= AppFunctions::appUrl("/logout"); ?>">Sair</a>
                        <span class="icon-menu icon-notext mobile_menu transition"></span>
                    </div>
                </div>
            </div>

            <div class="notification_center"></div>
        </div>

        <div class="dash_content_box">
            <?= $v->section("content"); ?>
        </div>
    </section>
</div>

<script src="<?= Functions::url("/shared/scripts/jquery.min.js"); ?>"></script>
<script src="<?= Functions::url("/shared/scripts/jquery.form.js"); ?>"></script>
<script src="<?= Functions::url("/shared/scripts/jquery-ui.js"); ?>"></script>
<script src="<?= Functions::url("/shared/scripts/jquery.mask.js"); ?>"></script>
<script src="<?= Functions::url("/shared/scripts/tinymce/tinymce.min.js"); ?>"></script>
<script src="<?= Functions::url("/shared/scripts/izimodal/js/iziModal.min.js"); ?>"></script>
<script src="<?= Functions::url("/vendor/select2/select2/dist/js/select2.min.js"); ?>"></script>
<script src="<?= Functions::theme("/assets/js/scripts.js", Config::VIEW_ADMIN); ?>"></script>
<?= $v->section("scripts"); ?>

</body>
</html>