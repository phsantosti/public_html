<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <?= $head; ?>

    <link rel="stylesheet" href="<?= \Application\Helpers\Functions::url("/shared/styles/boot.css"); ?>"/>
    <link rel="stylesheet" href="<?= \Application\Helpers\Functions::url("/shared/styles/styles.css"); ?>"/>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= \Application\Helpers\Functions::theme("/assets/css/login.css", \Application\Config\Config::VIEW_ADMIN); ?>"/>

    <link rel="icon" type="image/png" href="<?= \Application\Helpers\Functions::theme("/assets/images/favicon.png", \Application\Config\Config::VIEW_ADMIN); ?>"/>
</head>
<body>

<div class="ajax_load">
    <div class="ajax_load_box">
        <div class="ajax_load_box_circle"></div>
        <p class="ajax_load_box_title">Aguarde, carregando...</p>
    </div>
</div>

<?= $v->section("content"); ?>

<script src="<?= \Application\Helpers\Functions::url("/shared/scripts/jquery.min.js"); ?>"></script>
<script src="<?= \Application\Helpers\Functions::url("/shared/scripts/jquery-ui.js"); ?>"></script>
<script src="<?= \Application\Helpers\Functions::theme("/assets/js/login.js", \Application\Config\Config::VIEW_ADMIN); ?>"></script>

</body>
</html>
