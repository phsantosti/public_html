<div class="dash_content_sidebar">
    <h3 class="icon-asterisk">E-books</h3>
    <p class="dash_content_sidebar_desc">Gerencie os e-books e páginas de E-books do site...</p>

    <nav>
        <?php
            $app = "";
            $nav = function ($icon, $href, $title) use ($app) {
                $active = ($app == $href ? "active" : null);
                $url = \Application\Helpers\AppFunctions::appUrl("/{$href}");
                return "<a class=\"icon-{$icon} radius {$active}\" href=\"{$url}\" title=\"{$title}\">{$title}</a>";
            };

            echo $nav("leanpub", "ebooks", "E-books");
            echo $nav("plus", "ebooks/ebook", "Novo E-book");
            echo $nav("tags", "pages/pages-ebooks", "Páginas de E-books");
            echo $nav("tag", "pages/pages-ebooks/page-ebook", "Nova Página de E-book");
        ?>
    </nav>
</div>