<?php $v->layout("_admin"); ?>
<?php $v->insert("widgets/dash/sidebar.php"); ?>
<?php
use Application\Helpers\Functions;
?>
<section class="dash_content_app">
    <header class="dash_content_app_header">
        <h2 class="icon-home">Dashboard</h2>
    </header>

    <div class="dash_content_app_box">
        <section class="app_dash_home_stats">
            <article class="control radius">
                <h4 class="icon-bookmark-o">E-books</h4>
                <p><b>E-books Ativos:</b> <?= $ebooks->ebooks;?></p>
                <p><b>E-books Páginas Ativas:</b> <?= $ebooks->pages_ebooks;?></p>
            </article>

            <article class="control radius">
                <h4 class="icon-bookmark-o">Folders</h4>
                <p><b>Folders Ativos:</b> <?= $folders->folders;?></p>
                <p><b>Folders Páginas Ativas:</b> <?= $folders->pages_folders;?></p>
            </article>

            <article class="users radius">
                <h4 class="icon-user">Usuários</h4>
                <p><b>Usuários:</b> <?= $users->users; ?></p>
                <p><b>Admins:</b> <?= $users->admins; ?></p>
            </article>
        </section>
    </div>
</section>

<?php $v->start("scripts"); ?>
<script>
    $(function () {

    });
</script>
<?php $v->end(); ?>
