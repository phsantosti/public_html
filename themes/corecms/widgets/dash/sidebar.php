<div class="dash_content_sidebar">
    <h3 class="icon-asterisk">Dashboard</h3>
    <p class="dash_content_sidebar_desc">Tenha insights poderosos para escalar seus resultaods...</p>

    <nav>
        <?php
        $app = "";
        $nav = function ($icon, $href, $title) use ($app) {
            $active = ($app == $href ? "active" : null);
            $url = \Application\Helpers\AppFunctions::appUrl("/{$href}");
            return "<a class=\"icon-{$icon} radius {$active}\" href=\"{$url}\" title=\"{$title}\">{$title}</a>";
        };

        echo $nav("cog", "dashboard", "Dashborad");
        ?>
    </nav>
</div>