<?php $v->layout("_login"); ?>

<div class="login">
    <article class="login_box radius">
        <h1 class="hl">CoreCMS</h1>
        <div class="ajax_response"><?= \Application\Helpers\Functions::flash(); ?></div>

        <form name="login" action="<?= \Application\Helpers\AppFunctions::appUrl('/login'); ?>" method="post">
            <label>
                <span class="field icon-envelope">E-mail:</span>
                <?= \Application\Helpers\Functions::csrfInput();?>
                <input name="email" type="email" placeholder="Informe seu e-mail" required/>
            </label>

            <label>
                <span class="field icon-unlock-alt">Senha:</span>
                <input name="password" type="password" placeholder="Informe sua senha:" required/>
            </label>

            <button class="radius gradient gradient-corecms gradient-hover icon-sign-in">Entrar</button>
        </form>

        <footer>
            <p>Desenvolvido por www.<b>pedrohsantos</b>.com.br</p>
            <p>&copy; <?= date("Y"); ?> - todos os direitos reservados</p>
            <a target="_blank" class="icon-whatsapp transition" href="https://api.whatsapp.com/send?phone=5531975701624&text=Olá, preciso de ajuda com o login.">WhatsApp: (31) 97570.1624</a>
        </footer>
    </article>
</div>