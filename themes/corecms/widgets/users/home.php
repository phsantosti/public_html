<?php $v->layout("_admin"); ?>
<?php $v->insert("widgets/users/sidebar.php"); ?>
<?php
    use Application\Helpers\Functions;
    use Application\Helpers\AppFunctions;
    use Application\Config\Config;
?>
<section class="dash_content_app">
    <header class="dash_content_app_header">
        <h2 class="icon-user">Usuários</h2>
        <form action="<?= AppFunctions::appUrl("/users"); ?>" class="app_search_form">
            <input type="text" name="s" value="" placeholder="Pesquisar Usuário:">
            <button class="icon-search icon-notext"></button>
        </form>
    </header>

    <div class="dash_content_app_box">
        <section>
            <div class="app_users_home">
                <?php foreach ($users as $user): $userImage = ($user->avatar ? AppFunctions::image($user->avatar, 300, 300) : Functions::theme("/assets/images/avatar.jpg", Config::VIEW_ADMIN)); ?>
                    <article class="user radius">
                        <div class="cover" style="background-image: url(<?= $userImage; ?>)"></div>
                        <?php if ($user->level >= 3): ?>
                            <p class="level icon-life-ring">ADMIN</p>
                        <?php else: ?>
                            <p class="level icon-user">USUÁRIO</p>
                        <?php endif; ?>

                        <h4><?= $user->fullName(); ?></h4>
                        <div class="info">
                            <p><?= $user->email; ?></p>
                            <p>Desde <?= Functions::dateFormat($user->created_at, "d/m/y \à\s H\hi"); ?></p>
                        </div>

                        <div class="actions">
                            <a class="icon-cog btn btn-blue" href="<?= AppFunctions::appUrl("/users/user/{$user->id}"); ?>"
                               title="">Gerenciar</a>
                        </div>
                    </article>
                <?php endforeach; ?>
            </div>

            <?= $paginator; ?>
        </section>
    </div>
</section>