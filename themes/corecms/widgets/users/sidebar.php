<?php
    use Application\Helpers\AppFunctions;
?>
<div class="dash_content_sidebar">
    <h3 class="icon-asterisk">Dashboard/Usuários</h3>
    <p class="dash_content_sidebar_desc">Gerencie, monitore e acompanhe os usuários do seu site aqui...</p>

    <nav>
        <?php
        $app = "";
        $nav = function ($icon, $href, $title) use ($app) {
            $active = ($app == $href ? "active" : null);
            $url = AppFunctions::appUrl("/{$href}");
            return "<a class=\"icon-{$icon} radius {$active}\" href=\"{$url}\" title=\"{$title}\">{$title}</a>";
        };

        echo $nav("user", "users", "Usuários");
        echo $nav("plus-circle", "users/user", "Novo Usuário");
        ?>

        <?php if (!empty($user) && $user->avatar): ?>
            <img alt="<?= $user->fullName();?>" class="radius" style="width: 100%; margin-top: 30px" src="<?= AppFunctions::image($user->avatar, 600, 600); ?>"/>
        <?php endif; ?>
    </nav>
</div>