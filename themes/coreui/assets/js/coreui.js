$(function () {
    //FORM ENVIO PADRÃO
    $(".formSend").submit(function (e) {
        e.preventDefault();
        var form = $(this);
        var url = form.attr("action");
        var type = form.attr("method");

        if (typeof tinyMCE !== 'undefined') {
            tinyMCE.triggerSave();
        }

        form.ajaxSubmit({
            url: url,
            type: type,
            dataType: "json",
            beforeSend: function () {
                var options = {
                    theme: "sk-rect",
                    message: "Aguarde, processando informações..."
                };
                HoldOn.open(options);
            },
            success: function (response) {
                //success
                if(response.success){
                    //redirect
                    if(response.redirect){
                        window.location.href = response.redirect;
                    }else{
                        form.find("input[type='file']").val(null);
                    }

                    //reaload
                    if(response.reload){
                        window.location.reload();
                    }

                    if(response.message){
                        toastr.success(response.message, response.title);
                    }

                    if(response.mce_image){
                        $('.mce_upload').fadeOut(200);
                        tinyMCE.activeEditor.insertContent(response.mce_image);
                    }
                }else{
                    //redirect
                    if(response.redirect){
                        window.location.href = response.redirect;
                    }else{
                        form.find("input[type='file']").val(null);
                    }

                    //reaload
                    if(response.reaload){
                        window.location.reload();
                    }

                    if(response.message){
                        toastr.error(response.message, response.title);
                    }
                }
            },
            error: function () {
                toastr.error("Não foi possível processar sua requisição", "Ops!");
            },
            complete: function () {
                if (form.data("reset") === true) {
                    form.trigger("reset");
                }
                HoldOn.close();
            }
        });
    });

    // SET SELECT2 ALL
    if($(".setSelectTwo").length){
        $(".setSelectTwo").select2({
            theme: 'bootstrap4'
        });
    }

    //SET DATATABLE ALL
    if($(".setDataTableAll").length){
        $(".setDataTableAll").DataTable();
    }

    // CONTADOR DE NOTIFICAÇÕES - FUNÇÃO
    function notificationsCount() {
        var center = $(".notification_center_open");
        $.post(center.data("count"), function (response) {
            if (response.count) {
                center.html(response.count);
            } else {
                center.html("0");
            }
        }, "json");
    }

    // CONTA NOTIFICAÇÃO AO ACESSAR A PAGINA
    notificationsCount();

    // CONTA NOTIFICAÇÕES EM INTERVALOS
    setInterval(function () {
        notificationsCount();
    }, 1000 * 50);


});

function validatePasswordChange(inputPassword, inputPasswordConfirm) {
    if(inputPassword.val() != inputPasswordConfirm.val()){
        $("#validatePasswordMessage").removeClass('alert alert-success').addClass('alert alert-danger').html("Senha não confere").show();
    }else{
        $("#validatePasswordMessage").removeClass('alert alert-danger').addClass('alert alert-success').html("Senha confere").show();
    }
}