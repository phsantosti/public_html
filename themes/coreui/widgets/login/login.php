<?php $v->layout("_login"); ?>

<div class="card-group">
    <div class="card p-4">
        <div class="card-body">
            <h1 class="text-center">CoreCMS</h1>
            <p class="text-muted text-center">Gerenciador de Conteúdo Web Modular</p>
            <form action="<?= \Application\Helpers\AppFunctions::appUrl('/login');?>" method="post" class="formSend">
                <?= \Application\Helpers\Functions::csrfInput();?>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="icon-envelope"></i>
                        </span>
                    </div>
                    <input class="form-control" type="email" placeholder="E-mail" required="required" name="email" id="email">
                </div>
                <div class="input-group mb-4">
                    <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="icon-lock"></i>
                        </span>
                    </div>
                    <input class="form-control" type="password" placeholder="Senha" name="password" id="password" required="required">
                </div>
                <div class="row">
                    <div class="col-6">
                        <button class="btn btn-primary px-4" type="submit">Entrar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>