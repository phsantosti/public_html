<?php $v->layout("_admin"); ?>
<div class="row">
    <div class="col-sm-6 col-lg-3">
        <div class="card text-white bg-primary">
            <div class="card-body">
                <div class="text-muted font-weight-bold text-uppercase text-center">E-books Ativos</div>
                <div class="text-value text-center text-muted font-weight-bold text-uppercase"><?= (empty($ebookCount) ? 0 : $ebookCount);?></div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-lg-3">
        <div class="card text-white bg-primary">
            <div class="card-body">
                <div class="text-muted font-weight-bold text-uppercase text-center">Folders Ativos</div>
                <div class="text-value text-center text-muted font-weight-bold text-uppercase"><?= (empty($folderCount) ? 0 : $folderCount);?></div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-lg-3">
        <div class="card text-white bg-primary">
            <div class="card-body">
                <div class="text-muted font-weight-bold text-uppercase text-center">Artigos Publicados</div>
                <div class="text-value text-center text-muted font-weight-bold text-uppercase"><?= (empty($postCount) ? 0 : $postCount);?></div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-lg-3">
        <div class="card text-white bg-primary">
            <div class="card-body">
                <div class="text-muted font-weight-bold text-uppercase text-center">Cursos Ativos</div>
                <div class="text-value text-center text-muted font-weight-bold text-uppercase"><?= (empty($courseCount) ? 0 : $courseCount);?></div>
            </div>
        </div>
    </div>
</div>
