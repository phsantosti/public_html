<?php $v->layout("_admin"); ?>
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="icon-people"></i> Lista de Usuários do Sistema
                <button type="button" class="btn btn-success ml-auto mr-0 float-right" title="Cadastrar novo usuário do sistema" onclick="newUser();">
                    <i class="icon-plus"></i> Novo
                </button>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-sm table-striped table-hover table-bordered table-lista-usuarios" style="width: 100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nome Completo</th>
                                <th>E-mail</th>
                                <th>Nível de Acesso</th>
                                <th>Status</th>
                                <th>Data Cadastro</th>
                                <th>Data Atualizado</th>
                                <th>Opções</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Nome Completo</th>
                                <th>E-mail</th>
                                <th>Nível de Acesso</th>
                                <th>Status</th>
                                <th>Data Cadastro</th>
                                <th>Data Atualizado</th>
                                <th>Opções</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="card-footer"></div>
        </div>
    </div>
</div>

<!--MODAL NOVO USUARIO-->
<div class="remodal" id="remodalNewUser" data-remodal-options="closeOnOutsideClick: false">
    <button data-remodal-action="close" class="remodal-close"></button>
    <h1>Novo Usuário</h1>
    <div class="container p-5">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <img alt="Avatar" src="<?= \Application\Helpers\Functions::theme('/assets/images/avatar.jpg', \Application\Config\Config::VIEW_ADMIN);?>" class="img-fluid" style="max-width: 150px !important;" id="newAvatarImage">
                <form class="formSend" id="formNewUser" action="" method="post" enctype="multipart/form-data">
                    <div class="row mb-3">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <label for="newName">Nome</label>
                            <input type="text" class="form-control" id="newName" name="name" placeholder="Nome" required="required">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <label for="newLastname">Sobrenome</label>
                            <input type="text" class="form-control" id="newLastname" name="lastname" placeholder="Sobrenome" required="required">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <label for="newEmail">E-mail</label>
                            <input type="email" class="form-control" id="newEmail" name="email" placeholder="E-mail" required="required">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <label for="newPassword">Senha</label>
                            <input type="password" class="form-control" id="newPassword" name="password" placeholder="Senha" required="required">
                        </div>

                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <label for="newAvatar">Avatar</label>
                            <input type="file" class="form-control" id="newAvatar" name="avatar" placeholder="Avatar">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <label for="newLevel">Nível de Acesso</label>
                            <select class="form-control" id="newLevel" name="level" required="required">
                                <?php foreach (\Application\Helpers\AppFunctions::levels() as $level):?>
                                    <option value="<?= $level->id;?>"><?= $level->name;?></option>
                                <?php endforeach?>
                            </select>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <label for="newStatus">Status</label>
                            <select class="form-control" id="newStatus" name="status" required="required">
                                <option value="1">Ativo</option>
                                <option value="0">Inativo</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <button data-remodal-action="cancel" class="btn btn-link"><i class="icon-close"></i> Cancelar</button>
                            <button type="submit" class="btn btn-outline-success"><i class="icon-refresh"></i> Salvar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!--MODAL EDITAR USUARIO-->
<div class="remodal" id="remodalEditUser" data-remodal-options="closeOnOutsideClick: false">
    <button data-remodal-action="close" class="remodal-close"></button>
    <h1>Editar Usuário</h1>
    <div class="container p-5">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <img alt="Avatar" src="<?= \Application\Helpers\Functions::theme('/assets/images/avatar.jpg', \Application\Config\Config::VIEW_ADMIN);?>" class="img-fluid" style="max-width: 150px !important;" id="editAvatarImage">
                <form class="formSend" id="formEditUser" action="" method="post" enctype="multipart/form-data">
                    <div class="row mb-3">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <label for="editName">Nome</label>
                            <input type="text" class="form-control" id="editName" name="name" placeholder="Nome" required="required">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <label for="editLastname">Sobrenome</label>
                            <input type="text" class="form-control" id="editLastname" name="lastname" placeholder="Sobrenome" required="required">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <label for="editEmail">E-mail</label>
                            <input type="email" class="form-control" id="editEmail" name="email" placeholder="E-mail" required="required">
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <label for="editAvatar">Avatar</label>
                            <input type="file" class="form-control" id="editAvatar" name="avatar" placeholder="Avatar">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <label for="editLevel">Nível de Acesso</label>
                            <select class="form-control" id="editLevel" name="level" required="required">
                                <?php foreach (\Application\Helpers\AppFunctions::levels() as $level):?>
                                    <option value="<?= $level->id;?>"><?= $level->name;?></option>
                                <?php endforeach?>
                            </select>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <label for="editStatus">Status</label>
                            <select class="form-control" id="editStatus" name="status" required="required">
                                <option value="1">Ativo</option>
                                <option value="0">Inativo</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <button data-remodal-action="cancel" class="btn btn-link"><i class="icon-close"></i> Cancelar</button>
                            <button type="submit" class="btn btn-outline-success"><i class="icon-refresh"></i> Salvar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!--MODAL DELETAR USUARIO-->
<div class="remodal" id="remodalDeleteUser" data-remodal-options="closeOnOutsideClick: false">
    <button data-remodal-action="close" class="remodal-close"></button>
    <h1>Deletar Usuário</h1>
    <div class="container p-5">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <img alt="Avatar" src="<?= \Application\Helpers\Functions::theme('/assets/images/avatar.jpg', \Application\Config\Config::VIEW_ADMIN);?>" class="img-fluid" style="max-width: 150px !important;" id="deleteAvatarImage">
                <form class="formSend" id="formDeleteUser" action="" method="post" enctype="multipart/form-data">
                    <div class="row mb-3">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <span class="text-uppercase font-weight-bold text-center" id="deleteName"></span>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <button data-remodal-action="cancel" class="btn btn-link"><i class="icon-close"></i> Cancelar</button>
                            <button type="submit" class="btn btn-outline-danger"><i class="icon-refresh"></i> Deletar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!--MODAL ALTERAR SENHA USUARIO-->
<div class="remodal" id="remodalChangePasswordUser" data-remodal-options="closeOnOutsideClick: false">
    <button data-remodal-action="close" class="remodal-close"></button>
    <h1>Alterar Senha</h1>
    <div class="container p-5">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <form class="formSend" id="formChangePasswordUser" action="" method="post" enctype="multipart/form-data">
                    <div class="row mb-3">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <p class="" id="validatePasswordMessage" style="display: none;"></p>
                            <input type="password" class="form-control" id="editPassword" name="password" placeholder="Senha">
                            <input type="password" class="form-control" id="editPasswordConfirm" name="passwordConfirm" placeholder="Confirma Senha">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <button data-remodal-action="cancel" class="btn btn-link"><i class="icon-close"></i> Cancelar</button>
                            <button type="submit" class="btn btn-outline-success"><i class="icon-refresh"></i> Salvar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php $v->start("scripts"); ?>
<script>
    //READ USER
    $(function () {
        $('.table-lista-usuarios').DataTable({
            "language": {
                "url": "<?= \Application\Helpers\Functions::theme('/assets/js/pt-br.json', \Application\Config\Config::VIEW_ADMIN);?>"
            },
            "ajax": "<?= \Application\Helpers\AppFunctions::appUrl('/users/getUsers');?>",
            "columns":[
                { "data": "id" },
                { "data": "full_name" },
                { "data": "email" },
                { "data": "level" },
                { "data": "status" },
                { "data": "created_at" },
                { "data": "updated_at" },
                { "data": "options" }
            ]
        });
        setTimeout(function () {
            console.log('Lista de usuários carregada.');
            $('[data-toggle="tooltip"]').tooltip();
            console.log('Tool tips carregados.');
        }, 10000);
    });

    //CREATE USER
    function newUser() {
        HoldOn.open();
        $("#newAvatarImage").attr('src', '<?= \Application\Helpers\Functions::theme('/assets/images/avatar.jpg', \Application\Config\Config::VIEW_ADMIN);?>');
        $('#formNewUser').each(function () {
            $(this).trigger('reset');
        });

        //SET ACTION TO FORM
        $("#formNewUser").attr('action', '<?= \Application\Helpers\AppFunctions::appUrl("/users/user")?>');

        var remodalNewUser = $("#remodalNewUser").remodal();
        remodalNewUser.open();
        HoldOn.close();

    }

    //UPDATE USER
    function editUser(id) {
        HoldOn.open();
        $("#editAvatarImage").attr('src', '<?= \Application\Helpers\Functions::theme('/assets/images/avatar.jpg', \Application\Config\Config::VIEW_ADMIN);?>');
        $('#formEditUser').each(function () {
            $(this).trigger('reset');
        });
        $.getJSON("<?= \Application\Helpers\AppFunctions::appUrl('/users/user/');?>" + id, function (response) {
            if(response.success){
                //SET USER FIELDS TO UPDATE
                $("#editAvatarImage").attr('src', response.user.avatar);
                $("#editName").val(response.user.name);
                $("#editLastname").val(response.user.lastname);
                $("#editEmail").val(response.user.email);
                $("#editLevel").val(response.user.level);
                $("#editStatus").val(response.user.status);

                //SET ACTION TO FORM
                $("#formEditUser").attr('action', response.action);

                var remodalEditUser = $("#remodalEditUser").remodal();
                remodalEditUser.open();
            }else{
                toastr.error(response.message, response.title);
            }

        }).always(function() {
            HoldOn.close();
        });
    }

    //DELETE USER
    function deleteUser(id) {
        HoldOn.open();
        $("#deleteAvatarImage").attr('src', '<?= \Application\Helpers\Functions::theme('/assets/images/avatar.jpg', \Application\Config\Config::VIEW_ADMIN);?>');
        $('#formDeleteUser').each(function () {
            $(this).trigger('reset');
        });
        $.getJSON("<?= \Application\Helpers\AppFunctions::appUrl('/users/user/');?>" + id, function (response) {
            if(response.success){
                //SET USER TO DELETE
                $("#deleteAvatarImage").attr('src', response.user.avatar);
                $("#deleteName").html(response.user.name + ' ' + response.user.lastname);

                //SET ACTION TO FORM
                $("#formDeleteUser").attr('action', '<?= \Application\Helpers\AppFunctions::appUrl("/users/delete/")?>' + id);

                var remodalDeleteUser = $('#remodalDeleteUser').remodal();
                remodalDeleteUser.open();

            }else{
                toastr.error(response.message, response.title);
            }
        }).always(function () {
           HoldOn.close();
        });
    }

    //CHANGE PASSWORD USER
    function changePasswordUser(id) {
        $("#validatePasswordMessage").hide();
        HoldOn.open();
        $('#formChangePasswordUser').each(function () {
            $(this).trigger('reset');
        });
        $.getJSON("<?= \Application\Helpers\AppFunctions::appUrl('/users/user/');?>" + id, function (response) {
            if(response.success){
                $("#formChangePasswordUser").attr('action', '<?= \Application\Helpers\AppFunctions::appUrl("/users/user/changePassword/")?>' + id);
                var remodalChangePasswordUser = $('#remodalChangePasswordUser').remodal();
                remodalChangePasswordUser.open();
            }
        }).always(function () {
            HoldOn.close();
        });
    }

    //VIEW IMAGE EDIT AVATAR
    $('#editAvatar').on('change', function () {
        var reader = new FileReader();
        reader.onload = function(e){
            $('#editAvatarImage').attr('src', e.target.result);
        };
        if(this.files[0] != null){
            reader.readAsDataURL(this.files[0]);
        }else{
            reader.readAsDataURL("<?= \Application\Helpers\Functions::theme('/assets/images/avatar.jpg', \Application\Config\Config::VIEW_ADMIN);?>");
        }

    });

    //VIEW IMAGE CREATE AVATAR
    $('#newAvatar').on('change', function () {
       var reader = new FileReader();
       reader.onload = function (e) {
         $('#newAvatarImage').attr('src', e.target.result);
       };
        if(this.files[0] != null){
            reader.readAsDataURL(this.files[0]);
        }else{
            reader.readAsDataURL("<?= \Application\Helpers\Functions::theme('/assets/images/avatar.jpg', \Application\Config\Config::VIEW_ADMIN);?>");
        }
    });

    //CONFIRM ACTION SUBMIT USER
    $('.formSend').submit(function (e) {
        if($("#remodalEditUser").length){
            var remodalEditUser = $("#remodalEditUser").remodal();
            remodalEditUser.close();
        }

        if($("#remodalDeleteUser").length){
            var remodalDeleteUser = $("#remodalDeleteUser").remodal();
            remodalDeleteUser.close();
        }

        if($('#remodalChangePasswordUser').length){
            var remodalChangePasswordUser = $('#remodalChangePasswordUser').remodal();
            remodalChangePasswordUser.close();
        }

        if($('#remodalNewUser').length){
            var remodalNewUser = $('#remodalNewUser').remodal();
            remodalNewUser.close();
        }
    });

    //VALIDA SENHA HTML5
    $("#editPassword").on("change", function () {
        validatePasswordChange($(this), $("#editPasswordConfirm"));
    });

    //VALIDA/CONFIRMA SENHA HTML5
    $("#editPasswordConfirm").on("keyup", function () {
        validatePasswordChange($("#editPassword"), $(this));
    });

    //APRESENTA MENSAGEM DE AVISO SE EXISTIR
    <?php if(\Application\Helpers\Functions::session()->has('flash')):?>
        toastr.info("<?= \Application\Helpers\Functions::flash();?>");
    <?php endif;?>
</script>
<?php $v->end(); ?>
