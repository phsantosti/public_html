<?php $v->layout("_admin"); ?>
<?php $v->insert("widgets/users/sidebar.php"); ?>

<section class="dash_content_app">
    <?php if (!$user): ?>
        <header class="dash_content_app_header">
            <h2 class="icon-plus-circle">Novo Usuário</h2>
        </header>

        <div class="dash_content_app_box">
            <form class="app_form" action="<?= \Application\Helpers\AppFunctions::appUrl("/users/user"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="create"/>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Nome:</span>
                        <input type="text" name="name" placeholder="Primeiro nome" required/>
                    </label>

                    <label class="label">
                        <span class="legend">*Sobrenome:</span>
                        <input type="text" name="lastname" placeholder="Último nome" required/>
                    </label>
                </div>
                <label class="label">
                    <span class="legend">Foto: (600x600px)</span>
                    <input type="file" name="avatar"/>
                </label>
                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*E-mail:</span>
                        <input type="email" name="email" placeholder="Melhor e-mail" required/>
                    </label>

                    <label class="label">
                        <span class="legend">*Senha:</span>
                        <input type="password" name="password" placeholder="Senha de acesso" required/>
                    </label>
                </div>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Level:</span>
                        <select name="level" required class="setSelectTwo">
                            <?php if(!empty($levels)):?>
                                <?php foreach ($levels as $level):?>
                                    <option value="<?= $level->id;?>"><?= $level->name;?></option>
                                <?php endforeach;?>
                            <?php else:?>
                                <option value="3">Administrador Geral</option>
                            <?php endif;?>
                        </select>
                    </label>

                    <label class="label">
                        <span class="legend">*Status:</span>
                        <select name="status" required class="setSelectTwo">
                            <option value="0">Inativo</option>
                            <option value="1">Ativo</option>
                        </select>
                    </label>
                </div>

                <div class="al-right">
                    <button class="btn btn-green icon-check-square-o">Criar Usuário</button>
                </div>
            </form>
        </div>
    <?php else: ?>
        <header class="dash_content_app_header">
            <h2 class="icon-user"><?= $user->fullName(); ?></h2>
        </header>

        <div class="dash_content_app_box">
            <form class="app_form" action="<?= \Application\Helpers\AppFunctions::appUrl("/users/user/{$user->id}"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="update"/>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Nome:</span>
                        <input type="text" name="name" value="<?= $user->name; ?>"
                               placeholder="Primeiro nome" required/>
                    </label>

                    <label class="label">
                        <span class="legend">*Sobrenome:</span>
                        <input type="text" name="lastname" value="<?= $user->lastname; ?>" placeholder="Último nome"
                               required/>
                    </label>
                </div>
                <label class="label">
                    <span class="legend">Foto: (600x600px)</span>
                    <input type="file" name="avatar"/>
                </label>
                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*E-mail:</span>
                        <input type="email" name="email" value="<?= $user->email; ?>" placeholder="Melhor e-mail"
                               required/>
                    </label>

                    <label class="label">
                        <span class="legend">Alterar Senha:</span>
                        <input type="password" name="password" placeholder="Senha de acesso"/>
                    </label>
                </div>
                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Level:</span>
                        <select name="level" required class="setSelectTwo">
                            <option value="<?= $user->level;?>">Administrador Geral</option>
                            <?php if(!empty($levels)):?>
                                <?php foreach ($levels as $level):?>
                                    <option value="<?= $level->id;?>"><?= $level->name;?></option>
                                <?php endforeach;?>
                            <?php else:?>
                                <option value="3">Administrador Geral</option>
                            <?php endif;?>
                        </select>
                    </label>

                    <label class="label">
                        <span class="legend">*Status:</span>
                        <select name="status" required class="setSelectTwo">
                            <option value="<?= $user->status;?>"><?= (($user->status) ? "Ativo" : "Inativo");?></option>
                            <option value="0">Inativo</option>
                            <option value="1">Ativo</option>
                        </select>
                    </label>
                </div>

                <div class="app_form_footer">
                    <button class="btn btn-blue icon-check-square-o">Atualizar</button>
                    <a href="#" class="remove_link icon-warning"
                       data-post="<?= \Application\Helpers\AppFunctions::appUrl("/users/delete/{$user->id}"); ?>"
                       data-action="delete"
                       data-confirm="ATENÇÃO: Tem certeza que deseja excluir o usuário e todos os dados relacionados a ele? Esta ação não pode ser desfeita!"
                       data-user_id="<?= $user->id; ?>">Excluir Usuário</a>
                </div>
            </form>
        </div>
    <?php endif; ?>
</section>