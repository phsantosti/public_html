<?php $v->layout("_admin");

use Application\Helpers\Functions;
use Application\Models\EbookModel; ?>
<div class="row">
    <div class="col-lg-12 createEditEbookBlock" style="display: none;">
        <div class="card">
            <div class="card-header">
                <i class="icon-book-open"></i>
                <span class="font-weight-bold createEditEbookTittle">Cadastrar nova E-book</span>
            </div>
            <div class="card-body">
                <form action="" method="post" enctype="multipart/form-data" class="formSend" id="formSubmitEbook">
                    <div class="container">
                        <div class="row mb-3">
                            <div class="col-lg-12">
                                <label for="ebookTitle">Título:</label>
                                <input type="text" required="required" class="form-control" value="" id="ebookTitle" name="title">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-12">
                                <label for="ebookDescription">Descrição:</label>
                                <input type="text" required="required" class="form-control" value="" id="ebookDescription" name="description">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-12">
                                <label for="ebookImage">Capa: tamanho mínimo (373x476 pixels)</label>
                                <input type="file" required="required" class="form-control" value="" id="ebookImage" name="image">
                            </div>
                            <div class="col-lg-12 mt-3">
                                <label for="ebookLink">Arquivo PDF</label>
                                <input type="file" required="required" class="form-control" value="" id="ebookLink" name="link">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-12">
                                <label for="ebookStatus">Status</label>
                                <select required="required" class="form-control setSelectTwo" id="ebookStatus" name="status">
                                    <option value="1">Ativo</option>
                                    <option value="0">Inativo</option>
                                </select>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-12">
                                <button type="button" class="btn btn-danger" onclick="closeCreateEditEbookBlock();">
                                    <i class="icon-close"></i> Cancelar
                                </button>
                                <button type="submit" class="btn btn-success">
                                    <i class="icon-plus"></i> Salvar
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6" id="ebookImageReturn">

                        </div>
                        <div class="col-lg-6" id="ebookLinkReturn">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="card">
            <div class="card-header">
                <i class="icon-people"></i> Lista de E-books
                <button type="button" class="btn btn-success ml-auto mr-0 float-right" title="Cadastrar novo e-book de acesso do sistema" onclick="newEbook();">
                    <i class="icon-plus"></i> Novo
                </button>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-sm table-striped table-hover table-bordered" style="width: 100%" id="tableListEbooks">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Titulo</th>
                                <th>Arquivo</th>
                                <th>Downloads</th>
                                <th>Status</th>
                                <th>Data Cadastro</th>
                                <th>Data Atualização</th>
                                <th>Opções</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Titulo</th>
                                <th>Arquivo</th>
                                <th>Downloads</th>
                                <th>Status</th>
                                <th>Data Cadastro</th>
                                <th>Data Atualização</th>
                                <th>Opções</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="card-footer"></div>
        </div>
    </div>
</div>

<?php $v->start("scripts"); ?>
<script>
    $(function () {
        //READ EBOOKS
        $('#tableListEbooks').DataTable({
            "language": {
                "url": "<?= \Application\Helpers\Functions::theme('/assets/js/pt-br.json', \Application\Config\Config::VIEW_ADMIN);?>"
            },
            "ajax": "<?= \Application\Helpers\AppFunctions::appUrl('/ebooks/files/getEbooks');?>",
            "columns":[
                { "data": "id" },
                { "data": "title" },
                { "data": "link" },
                { "data": "downloads" },
                { "data": "status" },
                { "data": "created_at" },
                { "data": "updated_at" },
                { "data": "options" }
            ]
        });
        setTimeout(function () {
            $('[data-toggle="tooltip"]').tooltip();
            console.log('Tool tips carregados.');
        }, 5000);
    });

    //EDITAR EBOOK
    function editEbook(id) {
        HoldOn.open();

        $('.createEditEbookTittle').html("Editar E-book");
        $('#ebookTitle').val("");
        $('#ebookDescription').val("");
        $('#ebookImage').val(null);
        $('#ebookLink').val(null);
        $('#ebookStatus').val("1").change();
        $('#formSubmitEbook').attr('action', '').attr('method', '');
        $('#ebookImageReturn').html("");
        $('#ebookLinkReturn').html("");


        $.getJSON("<?= \Application\Helpers\AppFunctions::appUrl('/ebooks/files/file/read/');?>" + id, function (response) {
            console.log(response);

            if(response.success){
                //SET EBOOK FIELDS TO UPDATE
                $('#ebookTitle').val(response.ebook.title);
                $('#ebookDescription').val(response.ebook.description);
                $('#ebookImage').val(null);
                $('#ebookLink').val(null);
                $('#ebookStatus').val(response.ebook.status).change();
                $('#formSubmitEbook').attr('action', '<?= \Application\Helpers\AppFunctions::appUrl("/ebooks/files/file/update/");?>' + response.ebook.id).attr('method', 'post');
                $('.createEditEbookBlock').show();
                $('#ebookImageReturn').html(response.ebook.image);
                $('#ebookLinkReturn').html(response.ebook.link);

            }else{
                toastr.error(response.message, response.title);
            }

        }).always(function() {
            HoldOn.close();
        });
    }

    function deleteEbook(id) {
        bootbox.confirm({
            message: "Deseja realmente deletar este ebook?",
            buttons: {
                confirm: {
                    label: 'Sim, quero deletar',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Não, não quero deletar',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if(result == true){
                    HoldOn.open();
                    $.getJSON("<?= \Application\Helpers\AppFunctions::appUrl("/ebooks/files/file/delete/")?>" + id, function (response) {
                        console.log(response);

                        if(response.success){
                            toastr.success(response.message, response.title);
                            setTimeout(function () {
                                window.location.href = response.redirect;
                            }, 3000);
                        }else{
                            toastr.error(response.message, response.title);
                        }

                    }).always(function() {
                        HoldOn.close();
                    });
                }
            }
        });
    }
    
    function newEbook() {
        $('.createEditEbookTittle').html("Cadastrar novo E-book");
        $('#ebookId').val("");
        $('#ebookTitle').val("");
        $('#ebookDescription').val("");
        $('#ebookImage').val(null);
        $('#ebookLink').val(null);
        $('#ebookStatus').val("1").change();
        $('#formSubmitEbook').attr('action', '<?= \Application\Helpers\AppFunctions::appUrl("/ebooks/files/file/create")?>').attr('method', 'post');
        $('.createEditEbookBlock').show();
        $('#ebookImageReturn').html("");
        $('#ebookLinkReturn').html("");
    }
    
    function closeCreateEditEbookBlock() {
        $('.createEditEbookTittle').html("");
        $('#ebookId').val("");
        $('#ebookTitle').val("");
        $('#ebookDescription').val("");
        $('#ebookImage').val(null);
        $('#ebookLink').val(null);
        $('#ebookStatus').val("1").change();
        $('#formSubmitEbook').attr('action', '').attr('method', '');
        $('.createEditEbookBlock').hide();
        $('#ebookImageReturn').html("");
        $('#ebookLinkReturn').html("");
    }

    //APRESENTA MENSAGEM DE AVISO SE EXISTIR
    <?php if(\Application\Helpers\Functions::session()->has('flash')):?>
        toastr.info("<?= \Application\Helpers\Functions::flash();?>");
    <?php endif;?>
</script>
<?php $v->end(); ?>
