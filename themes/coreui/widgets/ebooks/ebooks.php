<?php
    use Application\Helpers\Functions;
    use Application\Helpers\AppFunctions;
    use Application\Config\Config;
?>
<?php $v->layout("_admin"); ?>
<?php $v->insert("widgets/ebooks/sidebar.php"); ?>
<section class="dash_content_app">
    <header class="dash_content_app_header">
        <h2 class="icon-user">E-Books</h2>
        <form action="<?= AppFunctions::appUrl("/ebooks"); ?>" class="app_search_form">
            <input type="text" name="s" value="" placeholder="Pesquisar E-Book:">
            <button class="icon-search icon-notext"></button>
        </form>
    </header>

    <div class="dash_content_app_box">
        <section>
            <div class="app_ebooks_home">
                <?php foreach ($ebooks as $ebook): $ebookImage = ($ebook->image ? AppFunctions::image($ebook->image, 300, 300) : Functions::theme("/assets/images/avatar.jpg", Config::VIEW_ADMIN)); ?>
                    <article class="user radius">
                        <div class="cover" style="background-image: url(<?= $ebookImage; ?>)"></div>
                        <h4><?= $ebook->title; ?></h4>
                        <div class="info">
                            <?= Functions::stringLimitWords($ebook->description,'10');?>
                        </div>

                        <div class="actions">
                            <a class="icon-cog btn btn-blue" href="<?= AppFunctions::appUrl("/ebooks/ebook/{$ebook->id}"); ?>"
                               title="">Gerenciar</a>
                        </div>
                    </article>
                <?php endforeach; ?>
            </div>


        </section>
    </div>
</section>