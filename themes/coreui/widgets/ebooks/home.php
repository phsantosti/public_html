<?php $v->layout("_admin"); ?>
<div class="row">
    <div class="col-lg-4 col-md-3 col-sm-12">
        <div class="card card-accent-primary">
            <div class="card-header">
                <i class="icon-notebook"></i> E-books
            </div>
            <img class="card-img-top" src="<?= \Application\Helpers\Functions::theme("/assets/images/ebook.jpg", \Application\Config\Config::VIEW_ADMIN); ?>" alt="E-books" title="E-books">
            <div class="card-footer">
                <p class="text-center">
                    <a href="<?= \Application\Helpers\AppFunctions::appUrl('/ebooks/files');?>" class="btn btn-outline-primary">
                        Acessar
                    </a>
                </p>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-3 col-sm-12">
        <div class="card card-accent-primary">
            <div class="card-header">
                <i class="icon-desktop"></i> E-books Páginas
            </div>
            <img class="card-img-top" src="<?= \Application\Helpers\Functions::theme("/assets/images/ebook.jpg", \Application\Config\Config::VIEW_ADMIN); ?>" alt="E-books" title="E-books">
            <div class="card-footer">
                <p class="text-center">
                    <a href="<?= \Application\Helpers\AppFunctions::appUrl('/ebooks/pages');?>" class="btn btn-outline-primary">
                        Acessar
                    </a>
                </p>
            </div>
        </div>
    </div>
</div>

<?php $v->start("scripts"); ?>
<script>

    //APRESENTA MENSAGEM DE AVISO SE EXISTIR
    <?php if(\Application\Helpers\Functions::session()->has('flash')):?>
        toastr.info("<?= \Application\Helpers\Functions::flash();?>");
    <?php endif;?>
</script>
<?php $v->end(); ?>
