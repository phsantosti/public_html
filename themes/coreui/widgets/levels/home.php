<?php $v->layout("_admin"); ?>
<div class="row">
    <div class="col-lg-12">

        <div class="card">
            <div class="card-header">
                <i class="icon-people"></i> Lista de Níveis de Acesso
                <button type="button" class="btn btn-success ml-auto mr-0 float-right" title="Cadastrar novo nível de acesso do sistema" onclick="newLevel();">
                    <i class="icon-plus"></i> Novo
                </button>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-sm table-striped table-hover table-bordered table-list-levels" style="width: 100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nome</th>
                                <th>Status</th>
                                <th>Data Cadastro</th>
                                <th>Data Atualizado</th>
                                <th>Opções</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Nome</th>
                                <th>Status</th>
                                <th>Data Cadastro</th>
                                <th>Data Atualizado</th>
                                <th>Opções</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="card-footer"></div>
        </div>
    </div>
</div>

<!--MODAL NOVO USUARIO-->
<div class="remodal" id="remodalNewLevel" data-remodal-options="closeOnOutsideClick: false">
    <button data-remodal-action="close" class="remodal-close"></button>
    <h1>Novo Nível de Acesso</h1>
    <div class="container p-5">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <form class="formSend" id="formNewLevel" action="" method="post" enctype="multipart/form-data">
                    <div class="row mb-3">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <label for="newName">Nome</label>
                            <input type="text" class="form-control" id="newName" name="name" placeholder="Nome" required="required">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <label for="newStatus">Status</label>
                            <select class="form-control" id="newStatus" name="status" required="required">
                                <option value="1">Ativo</option>
                                <option value="0">Inativo</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <button data-remodal-action="cancel" class="btn btn-link"><i class="icon-close"></i> Cancelar</button>
                            <button type="submit" class="btn btn-outline-success"><i class="icon-refresh"></i> Salvar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!--MODAL EDITAR USUARIO-->
<div class="remodal" id="remodalEditLevel" data-remodal-options="closeOnOutsideClick: false">
    <button data-remodal-action="close" class="remodal-close"></button>
    <h1>Editar Nível de Acesso</h1>
    <div class="container p-5">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <form class="formSend" id="formEditLevel" action="" method="post" enctype="multipart/form-data">
                    <div class="row mb-3">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <label for="editName">Nome</label>
                            <input type="text" class="form-control" id="editName" name="name" placeholder="Nome" required="required">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <label for="editStatus">Status</label>
                            <select class="form-control" id="editStatus" name="status" required="required">
                                <option value="1">Ativo</option>
                                <option value="0">Inativo</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <button data-remodal-action="cancel" class="btn btn-link"><i class="icon-close"></i> Cancelar</button>
                            <button type="submit" class="btn btn-outline-success"><i class="icon-refresh"></i> Salvar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!--MODAL DELETAR USUARIO-->
<div class="remodal" id="remodalDeleteLevel" data-remodal-options="closeOnOutsideClick: false">
    <button data-remodal-action="close" class="remodal-close"></button>
    <h1>Deletar Nível de Acesso</h1>
    <div class="container p-5">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <form class="formSend" id="formDeleteLevel" action="" method="post" enctype="multipart/form-data">
                    <div class="row mb-3">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <span class="text-uppercase font-weight-bold text-center" id="deleteName"></span>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <button data-remodal-action="cancel" class="btn btn-link"><i class="icon-close"></i> Cancelar</button>
                            <button type="submit" class="btn btn-outline-danger"><i class="icon-refresh"></i> Deletar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php $v->start("scripts"); ?>
<script>
    //READ USER
    $(function () {
        $('.table-list-levels').DataTable({
            "language": {
                "url": "<?= \Application\Helpers\Functions::theme('/assets/js/pt-br.json', \Application\Config\Config::VIEW_ADMIN);?>"
            },
            "ajax": "<?= \Application\Helpers\AppFunctions::appUrl('/levels/getLevels');?>",
            "columns":[
                { "data": "id" },
                { "data": "name" },
                { "data": "status" },
                { "data": "created_at" },
                { "data": "updated_at" },
                { "data": "options" }
            ]
        });
        setTimeout(function () {
            console.log('Lista de níveis de acesso carregada.');
            $('[data-toggle="tooltip"]').tooltip();
            console.log('Tool tips carregados.');
        }, 10000);
    });

    //CREATE USER
    function newLevel() {
        HoldOn.open();
        $('#formNewLevel').each(function () {
            $(this).trigger('reset');
        });

        //SET ACTION TO FORM
        $("#formNewLevel").attr('action', '<?= \Application\Helpers\AppFunctions::appUrl("/levels/level")?>');

        var remodalNewLevel = $("#remodalNewLevel").remodal();
        remodalNewLevel.open();
        HoldOn.close();

    }

    //UPDATE USER
    function editLevel(id) {
        HoldOn.open();
        $('#formEditLevel').each(function () {
            $(this).trigger('reset');
        });
        $.getJSON("<?= \Application\Helpers\AppFunctions::appUrl('/levels/level/');?>" + id, function (response) {
            if(response.success){
                //SET USER FIELDS TO UPDATE
                $("#editName").val(response.level.name);
                $("#editStatus").val(response.level.status);

                //SET ACTION TO FORM
                $("#formEditLevel").attr('action', response.action);

                var remodalEditLevel = $("#remodalEditLevel").remodal();
                remodalEditLevel.open();
            }else{
                toastr.error(response.message, response.title);
            }

        }).always(function() {
            HoldOn.close();
        });
    }

    //DELETE USER
    function deleteLevel(id) {
        HoldOn.open();
        $('#formDeleteLevel').each(function () {
            $(this).trigger('reset');
        });
        $.getJSON("<?= \Application\Helpers\AppFunctions::appUrl('/levels/level/');?>" + id, function (response) {
            console.log(response);
            if(response.success){
                //SET USER TO DELETE
                $("#deleteName").html(response.level.name);

                //SET ACTION TO FORM
                $("#formDeleteLevel").attr('action', '<?= \Application\Helpers\AppFunctions::appUrl("/levels/delete/")?>' + id);

                var remodalDeleteLevel = $('#remodalDeleteLevel').remodal();
                remodalDeleteLevel.open();

            }else{
                toastr.error(response.message, response.title);
            }
        }).always(function () {
           HoldOn.close();
        });
    }

    //CONFIRM ACTION SUBMIT USER
    $('.formSend').submit(function (e) {
        if($("#remodalEditLevel").length){
            var remodalEditLevel = $("#remodalEditLevel").remodal();
            remodalEditLevel.close();
        }

        if($("#remodalDeleteLevel").length){
            var remodalDeleteLevel = $("#remodalDeleteLevel").remodal();
            remodalDeleteLevel.close();
        }

        if($('#remodalNewLevel').length){
            var remodalNewLevel = $('#remodalNewLevel').remodal();
            remodalNewLevel.close();
        }
    });

    //APRESENTA MENSAGEM DE AVISO SE EXISTIR
    <?php if(\Application\Helpers\Functions::session()->has('flash')):?>
        toastr.info("<?= \Application\Helpers\Functions::flash();?>");
    <?php endif;?>
</script>
<?php $v->end(); ?>
