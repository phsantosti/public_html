<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <base href="<?= \Application\Helpers\AppFunctions::appUrl();?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <link rel="shortcut icon" href="<?= \Application\Helpers\Functions::theme('/assets/images/favicon.png', \Application\Config\Config::VIEW_ADMIN);?>"/>
        <?= $head;?>
        <!-- Icons-->
        <link href="<?= \Application\Helpers\Functions::theme('/node_modules/@coreui/icons/css/coreui-icons.min.css', \Application\Config\Config::VIEW_ADMIN)?>" rel="stylesheet">
        <link href="<?= \Application\Helpers\Functions::theme('/node_modules/flag-icon-css/css/flag-icon.min.css', \Application\Config\Config::VIEW_ADMIN)?>" rel="stylesheet">
        <link href="<?= \Application\Helpers\Functions::theme('/node_modules/font-awesome/css/font-awesome.min.css', \Application\Config\Config::VIEW_ADMIN)?>" rel="stylesheet">
        <link href="<?= \Application\Helpers\Functions::theme('/node_modules/simple-line-icons/css/simple-line-icons.css', \Application\Config\Config::VIEW_ADMIN)?>" rel="stylesheet">
        <!-- Main styles for this application-->
        <link href="<?= \Application\Helpers\Functions::theme('/assets/css/HoldOn.min.css', \Application\Config\Config::VIEW_ADMIN)?>" rel="stylesheet">
        <link href="<?= \Application\Helpers\Functions::theme('/assets/css/style.css', \Application\Config\Config::VIEW_ADMIN)?>" rel="stylesheet">
        <link href="<?= \Application\Helpers\Functions::theme('/assets/vendors/pace-progress/css/pace.min.css', \Application\Config\Config::VIEW_ADMIN)?>" rel="stylesheet">
        <link href="<?= \Application\Helpers\Functions::theme('/assets/css/dataTables.bootstrap4.min.css', \Application\Config\Config::VIEW_ADMIN)?>" rel="stylesheet">
        <link href="<?= \Application\Helpers\Functions::theme('/assets/css/toastr.min.css', \Application\Config\Config::VIEW_ADMIN)?>" rel="stylesheet">
        <link href="<?= \Application\Helpers\Functions::theme('/assets/css/select2.min.css', \Application\Config\Config::VIEW_ADMIN)?>" rel="stylesheet">
        <link href="<?= \Application\Helpers\Functions::theme('/assets/css/select2-bootstrap4.min.css', \Application\Config\Config::VIEW_ADMIN)?>" rel="stylesheet">
        <link href="<?= \Application\Helpers\Functions::theme('/assets/vendors/remodal/remodal.css', \Application\Config\Config::VIEW_ADMIN)?>" rel="stylesheet">
        <link href="<?= \Application\Helpers\Functions::theme('/assets/vendors/remodal/remodal-default-theme.css', \Application\Config\Config::VIEW_ADMIN)?>" rel="stylesheet">
    </head>
    <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
        <header class="app-header navbar">
            <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="#">
                <img class="navbar-brand-full" src="<?= \Application\Helpers\Functions::theme('/assets/images/logo-sistema.png', \Application\Config\Config::VIEW_ADMIN);?>" width="120" height="50" alt="CoreCMS - Gerenciador de Conteúdo Web Modular">
                <img class="navbar-brand-minimized" src="<?= \Application\Helpers\Functions::theme('/assets/images/favicon.png', \Application\Config\Config::VIEW_ADMIN);?>" width="30" height="30" alt="CoreCMS - Gerenciador de Conteúdo Web Modular">
            </a>
            <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
                <span class="navbar-toggler-icon"></span>
            </button>
            <ul class="nav navbar-nav d-md-down-none">
                <li class="nav-item px-3">
                    <a class="nav-link" href="<?= \Application\Helpers\AppFunctions::appUrl('/dashboard');?>">Dashboard</a>
                </li>
                <li class="nav-item px-3">
                    <a class="nav-link" href="<?= \Application\Helpers\AppFunctions::appUrl('/users');?>">Usuários</a>
                </li>
                <li class="nav-item px-3">
                    <a class="nav-link" href="<?= \Application\Helpers\AppFunctions::appUrl('/settings')?>">Configurações</a>
                </li>
            </ul>
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item d-md-down-none mr-5">
                    <a class="nav-link" href="<?= \Application\Helpers\AppFunctions::appUrl('/notifications');?>">
                        <i class="icon-bell"></i>
                        <span class="badge badge-pill badge-danger notification_center_open" data-count="<?= \Application\Helpers\AppFunctions::appUrl("/notifications/count"); ?>" data-notify="<?= \Application\Helpers\AppFunctions::appUrl("/notifications/list"); ?>">0</span>
                    </a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <span class="text-muted font-weight-bold"><?= \Application\Helpers\Functions::user()->fullName();?></span>
                        <img class="img-avatar" src="<?= \Application\Helpers\AppFunctions::image(\Application\Helpers\Functions::user()->avatar, 120);?>" alt="<?= \Application\Helpers\Functions::user()->fullName();?>" title="<?= \Application\Helpers\Functions::user()->fullName();?>">
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <div class="dropdown-header text-center">
                            <strong>Opções</strong>
                        </div>
                        <a class="dropdown-item" href="<?= \Application\Helpers\AppFunctions::appUrl('/users/user/'.\Application\Helpers\Functions::user()->id);?>"><i class="fa fa-user"></i> Meu Perfil</a>
                        <a class="dropdown-item" href="<?= \Application\Helpers\AppFunctions::appUrl('/settings');?>"><i class="fa fa-wrench"></i> Configurações</a>
                        <a class="dropdown-item" href="https://api.whatsapp.com/send?phone=5531975701624&text=Olá, preciso de ajuda com o sistema CoreCMS." target="_blank"><i class="fa fa-whatsapp"></i> Suporte Técnico</a>
                        <a class="dropdown-item bg-danger text-white" href="<?= \Application\Helpers\AppFunctions::appUrl('/logout');?>"><i class="fa fa-lock"></i> Sair</a>
                    </div>
                </li>
            </ul>
        </header>
        <div class="app-body">
            <div class="sidebar">
                <nav class="sidebar-nav">
                    <ul class="nav">
                        <li class="nav-title text-muted text-capitalize">Menu Principal</li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= \Application\Helpers\AppFunctions::appUrl('/dashboard');?>">
                                <i class="nav-icon icon-speedometer"></i> Dashboard
                            </a>
                        </li>
                        <li class="nav-title text-muted text-capitalize">Gestão de Conteúdo</li>
                        <li class="nav-item nav-dropdown">
                            <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon icon-note"></i> Blog</a>
                            <ul class="nav-dropdown-items">
                                <li class="nav-item"><a class="nav-link" href="<?= \Application\Helpers\AppFunctions::appUrl('/blog/categories');?>"><i class="nav-icon icon-tag"></i> Categorias</a></li>
                                <li class="nav-item"><a class="nav-link" href="<?= \Application\Helpers\AppFunctions::appUrl('/blog/posts')?>"><i class="nav-icon icon-pencil"></i> Artigos</a></li>
                            </ul>
                        </li>
                        <li class="nav-item nav-dropdown">
                            <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon icon-screen-tablet"></i> E-books</a>
                            <ul class="nav-dropdown-items">
                                <li class="nav-item"><a class="nav-link" href="<?= \Application\Helpers\AppFunctions::appUrl('/ebooks/files');?>"><i class="nav-icon icon-notebook"></i> E-books (Arquivos)</a></li>
                                <li class="nav-item"><a class="nav-link" href="<?= \Application\Helpers\AppFunctions::appUrl('/ebook/pages');?>"><i class="nav-icon icon-screen-desktop"></i> E-books (Páginas)</a></li>
                            </ul>
                        </li>
                        <li class="nav-item nav-dropdown">
                            <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon icon-screen-tablet"></i> Folders</a>
                            <ul class="nav-dropdown-items">
                                <li class="nav-item"><a class="nav-link" href="<?= \Application\Helpers\AppFunctions::appUrl('/folders/files');?>"><i class="nav-icon icon-notebook"></i> Folders (Arquivos)</a></li>
                                <li class="nav-item"><a class="nav-link" href="<?= \Application\Helpers\AppFunctions::appUrl('/folders/pages');?>"><i class="nav-icon icon-screen-desktop"></i> Folders (Páginas)</a></li>
                            </ul>
                        </li>
                        <li class="nav-item nav-dropdown">
                            <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon icon-control-play"></i> EAD</a>
                            <ul class="nav-dropdown-items">
                                <li class="nav-item"><a class="nav-link" href="<?= \Application\Helpers\AppFunctions::appUrl('/ead/courses');?>"><i class="nav-icon icon-grid"></i> Cursos</a></li>
                                <li class="nav-item"><a class="nav-link" href="<?= \Application\Helpers\AppFunctions::appUrl('/ead/modules');?>"><i class="nav-icon icon-grid"></i> Módulos</a></li>
                                <li class="nav-item"><a class="nav-link" href="<?= \Application\Helpers\AppFunctions::appUrl('/ead/disciplines');?>"><i class="nav-icon icon-grid"></i> Disciplinas</a></li>
                                <li class="nav-item"><a class="nav-link" href="<?= \Application\Helpers\AppFunctions::appUrl('/ead/lessons');?>"><i class="nav-icon icon-grid"></i> Aulas</a></li>
                                <li class="nav-item"><a class="nav-link" href="<?= \Application\Helpers\AppFunctions::appUrl('/ead/documents');?>"><i class="nav-icon icon-grid"></i> PDF's</a></li>
                            </ul>
                        </li>
                        <li class="nav-title text-muted text-capitalize">Gestão de Usuários</li>
                        <li class="nav-item nav-dropdown">
                            <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon icon-people"></i> Usuários</a>
                            <ul class="nav-dropdown-items">
                                <li class="nav-item"><a class="nav-link" href="<?= \Application\Helpers\AppFunctions::appUrl('/users');?>"><i class="nav-icon icon-user-follow"></i> Cadastro</a></li>
                                <li class="nav-item"><a class="nav-link" href="<?= \Application\Helpers\AppFunctions::appUrl('/levels');?>"><i class="nav-icon icon-chart"></i> Níveis de Acesso</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <button class="sidebar-minimizer brand-minimizer" type="button"></button>
            </div>
            <main class="main">
                <!-- Breadcrumb-->
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?= \Application\Helpers\AppFunctions::appUrl();?>">Sistema</a></li>
                    <li class="breadcrumb-item active"><?= $page_name;?></li>

                    <!-- Breadcrumb Menu-->
                    <li class="breadcrumb-menu d-md-down-none">
                        <div class="btn-group" role="group" aria-label="Button group">
                            <a class="btn" href="<?= \Application\Helpers\AppFunctions::appUrl('/contacts');?>"><i class="icon-speech"></i> Contato Site</a>
                            <a class="btn" href="<?= \Application\Helpers\AppFunctions::appUrl('/reports');?>"><i class="icon-graph"></i>  Relatórios</a>
                            <a class="btn" href="<?= \Application\Helpers\AppFunctions::appUrl('/settings');?>"><i class="icon-settings"></i>  Configurações</a>
                        </div>
                    </li>
                </ol>
                <div class="container-fluid">
                    <div class="animated fadeIn">
                        <?= $v->section("content"); ?>
                    </div>
                </div>
            </main>
        </div>
        <footer class="app-footer">
            <div>
                <a href="https://corecms.pedrohsantos.com.br" target="_blank">CoreCMS</a>
                <span>&copy; <?= date('Y');?> v1.0.2</span>
            </div>
            <div class="ml-auto">
                <span>Desenvolvidor por</span>
                <a href="https://www.pedrohsantos.com.br" target="_blank">Pedro H. Santos - Fullstack Web Developer</a>
            </div>
        </footer>
        <!-- CoreUI and necessary plugins-->
        <script src="<?= \Application\Helpers\Functions::theme('/node_modules/jquery/dist/jquery.min.js', \Application\Config\Config::VIEW_ADMIN)?>"></script>
        <script src="<?= \Application\Helpers\Functions::theme('/node_modules/popper.js/dist/umd/popper.min.js', \Application\Config\Config::VIEW_ADMIN)?>"></script>
        <script src="<?= \Application\Helpers\Functions::theme('/node_modules/bootstrap/dist/js/bootstrap.min.js', \Application\Config\Config::VIEW_ADMIN)?>"></script>
        <script src="<?= \Application\Helpers\Functions::theme('/node_modules/pace-progress/pace.min.js', \Application\Config\Config::VIEW_ADMIN)?>"></script>
        <script src="<?= \Application\Helpers\Functions::theme('/node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js', \Application\Config\Config::VIEW_ADMIN)?>"></script>
        <script src="<?= \Application\Helpers\Functions::theme('/node_modules/@coreui/coreui/dist/js/coreui.min.js', \Application\Config\Config::VIEW_ADMIN)?>"></script>
        <script src="<?= \Application\Helpers\Functions::theme('/assets/js/HoldOn.min.js', \Application\Config\Config::VIEW_ADMIN)?>"></script>
        <script src="<?= \Application\Helpers\Functions::theme('/assets/js/toastr.min.js', \Application\Config\Config::VIEW_ADMIN)?>"></script>
        <script src="<?= \Application\Helpers\Functions::theme('/assets/js/jquery.form.min.js', \Application\Config\Config::VIEW_ADMIN)?>"></script>
        <script src="<?= \Application\Helpers\Functions::theme('/assets/js/jquery.dataTables.min.js', \Application\Config\Config::VIEW_ADMIN)?>"></script>
        <script src="<?= \Application\Helpers\Functions::theme('/assets/js/dataTables.bootstrap4.min.js', \Application\Config\Config::VIEW_ADMIN)?>"></script>
        <script src="<?= \Application\Helpers\Functions::theme('/assets/js/select2.min.js', \Application\Config\Config::VIEW_ADMIN)?>"></script>
        <script src="<?= \Application\Helpers\Functions::theme('/assets/vendors/remodal/remodal.min.js', \Application\Config\Config::VIEW_ADMIN)?>"></script>
        <script src="<?= \Application\Helpers\Functions::theme('/assets/js/bootbox.min.js', \Application\Config\Config::VIEW_ADMIN)?>"></script>
        <script src="<?= \Application\Helpers\Functions::theme('/assets/js/coreui.js', \Application\Config\Config::VIEW_ADMIN)?>"></script>
        <?= $v->section("scripts"); ?>
    </body>
</html>
