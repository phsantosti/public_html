<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <base href="<?= \Application\Helpers\AppFunctions::appUrl();?>">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <link rel="shortcut icon" href="<?= \Application\Helpers\Functions::theme('/assets/images/favicon.png', \Application\Config\Config::VIEW_ADMIN);?>"/>
        <?= $head;?>
        <!-- Icons-->
        <link href="<?= \Application\Helpers\Functions::theme('/node_modules/@coreui/icons/css/coreui-icons.min.css', \Application\Config\Config::VIEW_ADMIN)?>" rel="stylesheet">
        <link href="<?= \Application\Helpers\Functions::theme('/node_modules/flag-icon-css/css/flag-icon.min.css', \Application\Config\Config::VIEW_ADMIN)?>" rel="stylesheet">
        <link href="<?= \Application\Helpers\Functions::theme('/node_modules/font-awesome/css/font-awesome.min.css', \Application\Config\Config::VIEW_ADMIN)?>" rel="stylesheet">
        <link href="<?= \Application\Helpers\Functions::theme('/node_modules/simple-line-icons/css/simple-line-icons.css', \Application\Config\Config::VIEW_ADMIN)?>" rel="stylesheet">
        <!-- Main styles for this application-->
        <link href="<?= \Application\Helpers\Functions::theme('/assets/css/HoldOn.min.css', \Application\Config\Config::VIEW_ADMIN)?>" rel="stylesheet">
        <link href="<?= \Application\Helpers\Functions::theme('/assets/css/style.css', \Application\Config\Config::VIEW_ADMIN)?>" rel="stylesheet">
        <link href="<?= \Application\Helpers\Functions::theme('/assets/vendors/pace-progress/css/pace.min.css', \Application\Config\Config::VIEW_ADMIN)?>" rel="stylesheet">
        <link href="<?= \Application\Helpers\Functions::theme('/assets/css/toastr.min.css', \Application\Config\Config::VIEW_ADMIN)?>" rel="stylesheet">
    </head>
    <body class="app flex-row align-items-center">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <?= $v->section("content"); ?>
                </div>
            </div>
        </div>
        <!-- CoreUI and necessary plugins-->
        <script src="<?= \Application\Helpers\Functions::theme('/node_modules/jquery/dist/jquery.min.js', \Application\Config\Config::VIEW_ADMIN)?>"></script>
        <script src="<?= \Application\Helpers\Functions::theme('/node_modules/popper.js/dist/umd/popper.min.js', \Application\Config\Config::VIEW_ADMIN)?>"></script>
        <script src="<?= \Application\Helpers\Functions::theme('/node_modules/bootstrap/dist/js/bootstrap.min.js', \Application\Config\Config::VIEW_ADMIN)?>"></script>
        <script src="<?= \Application\Helpers\Functions::theme('/node_modules/pace-progress/pace.min.js', \Application\Config\Config::VIEW_ADMIN)?>"></script>
        <script src="<?= \Application\Helpers\Functions::theme('/node_modules/perfect-scrollbar/dist/perfect-scrollbar.min.js', \Application\Config\Config::VIEW_ADMIN)?>"></script>
        <script src="<?= \Application\Helpers\Functions::theme('/node_modules/@coreui/coreui/dist/js/coreui.min.js', \Application\Config\Config::VIEW_ADMIN)?>"></script>
        <script src="<?= \Application\Helpers\Functions::theme('/assets/js/HoldOn.min.js', \Application\Config\Config::VIEW_ADMIN)?>"></script>
        <script src="<?= \Application\Helpers\Functions::theme('/assets/js/toastr.min.js', \Application\Config\Config::VIEW_ADMIN)?>"></script>
        <script src="<?= \Application\Helpers\Functions::theme('/assets/js/jquery.form.min.js', \Application\Config\Config::VIEW_ADMIN)?>"></script>
        <script src="<?= \Application\Helpers\Functions::theme('/assets/js/coreui.js', \Application\Config\Config::VIEW_ADMIN)?>"></script>
    </body>
</html>
