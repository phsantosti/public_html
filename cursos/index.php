<?php ob_start();

require __DIR__ . '/../vendor/autoload.php';

use CoffeeCode\Router\Router;
use Application\Libraries\Session;
use Application\Helpers\Functions;
use Application\Helpers\EbookFunctions;

$session = new Session();

$route = new Router(EbookFunctions::ebookUrl(), ':');

$route->namespace('Application\Controllers\Site');

$route->group(null);

$route->get("/", "EbookController:index");


$route->namespace("Application\Controllers\Site");

$route->group("/ops");

$route->get("/{errcode}", "EbookController:error");

$route->dispatch();

if($route->error()){
    $route->redirect("/ops/{$route->error()}");
}

ob_end_flush();
