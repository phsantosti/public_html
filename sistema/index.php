<?php ob_start();

require __DIR__ . '/../vendor/autoload.php';

use CoffeeCode\Router\Router;
use Application\Libraries\Session;
use Application\Helpers\Functions;
use Application\Helpers\EbookFunctions;

$session = new Session();

$route = new Router(\Application\Helpers\AppFunctions::appUrl(), ':');

$route->namespace('Application\Controllers\Application');

$route->group(null);

//LOGIN GET
$route->get("/", "LoginController:index");
$route->get("/logout", "LoginController:logout");
//LOGIN POST
$route->post("/login", "LoginController:login");

//DASHBOARD GET
$route->get("/dashboard", "DashboardController:index");

//USERS GET
$route->get("/users", "UserController:index");
$route->get("/users/user/{user_id}", "UserController:read");
$route->get("/users/getUsers", "UserController:read");
//USERS POST
$route->post("/users/user", "UserController:create");
$route->post("/users/user/{user_id}", "UserController:update");
$route->post("/users/user/changePassword/{user_id}", "UserController:changePassword");
$route->post("/users/delete/{user_id}", "UserController:delete");
//LEVELS GET
$route->get("/levels", "LevelController:index");
$route->get("/levels/level/{level_id}", "LevelController:read");
$route->get("/levels/getLevels", "LevelController:read");
//LEVELS POST
$route->post("/levels/level", "LevelController:create");
$route->post("/levels/level/{level_id}", "LevelController:update");
$route->post("/levels/delete/{level_id}", "LevelController:delete");
//NOTIFICATIONS POST
$route->post("/notifications/count", "NotificationController:count");
$route->post("/notifications/list", "NotificationController:list");
//E-BOOKS GET
$route->get("/ebooks", "EbookController:index");
//EBOOKS FILES GET
$route->get("/ebooks/files", "EbookController:ebookFiles");
$route->get("/ebooks/files/getEbooks", "EbookController:ebookReadFile");
$route->get("/ebooks/files/file/read/{ebook_id}", "EbookController:ebookReadFile");
$route->get("/ebooks/files/file/delete/{ebook_id}", "EbookController:ebookDeleteFile");
//EBOOKS FILES POST
$route->post("/ebooks/files/file/create", "EbookController:ebookCreateFile");
$route->post("/ebooks/files/file/update/{ebook_id}", "EbookController:ebookUpdateFile");
//EBOOKS PAGES GET
$route->get("/ebooks/pages", "EbookController:ebookPages");
$route->get("/ebooks/pages/getPages", "EbookController:ebookReadPage");
$route->get("/ebooks/pages/page/read/{ebook_page_id}", "EbookController:ebookReadPage");
$route->get("/ebooks/pages/page/delete/{ebook_page_id", "EbookController:ebookDeletePage");
//EBOOKS PAGES POST
$route->post("/ebook/pages/page/create", "EbookController:ebookCreatePage");
$route->post("/ebook/pages/page/update/{ebook_page_id}", "EbookController:ebookUploadPage");


$route->get("/blog/categories", "DashboardController:index");
$route->get("/blog/posts", "DashboardController:index");
$route->get("/folders/files", "DashboardController:index");
$route->get("/folders/pages", "DashboardController:index");
$route->get("/ead/courses", "DashboardController:index");
$route->get("/ead/modules", "DashboardController:index");
$route->get("/ead/disciplines", "DashboardController:index");
$route->get("/ead/lessons", "DashboardController:index");
$route->get("/ead/documents", "DashboardController:index");



////FOLDERS
//$route->get("/folders", "FolderController:index");
//$route->post("/folders/create", "FolderController:create");
//$route->get("/folders/read", "FolderController:read");
//$route->post("/folders/update", "FolderController:update");
//$route->post("/folders/delete", "FolderController:delete");
//
////PAGES EBOOKS
//$route->get("/pages-ebooks", "PageEbookController:index");
//$route->post("/pages-ebooks/create", "PageEbookController:create");
//$route->get("/pages-ebooks/read", "PageEbookController:read");
//$route->post("/pages-ebooks/update", "PageEbookController:update");
//$route->post("/pages-ebooks/delete", "PageEbookController:delete");
//
////PAGES FOLDERS
//$route->get("/pages-folders", "PageFolderController:index");
//$route->post("/pages-folders/create", "PageFolderController:create");
//$route->get("/pages-folders/read", "PageFolderController:read");
//$route->post("/pages-folders/update", "PageFolderController:update");
//$route->post("/pages-folders/delete", "PageFolderController:delete");
//
$route->namespace("Application\Controllers\Site");

$route->group("/ops");

$route->get("/{errcode}", "EbookController:error");

$route->dispatch();

if($route->error()){
    $route->redirect("/ops/{$route->error()}");
}

ob_end_flush();
