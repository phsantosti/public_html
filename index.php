<?php ob_start();

require __DIR__ . '/vendor/autoload.php';

use CoffeeCode\Router\Router;
use Application\Libraries\Session;
use Application\Helpers\Functions;

$session = new Session();

$route = new Router(Functions::url(), ':');

$route->namespace('Application\Controllers\Site');

$route->group(null);

$route->get("/", "WebController:index");
$route->get("/sobre", "WebController:about");
$route->get("/servicos", "WebController:services");
$route->get("/servicos/{service_uri}", "WebController:services");
$route->get("/portfolio", "WebController:portfolio");
$route->get("/blog", "WebController:blog");
$route->get("/blog/{category_uri}/{post_uri}", "WebController:blog");
$route->get("/contato", "WebController:contact");
$route->get("/termos", "WebController:terms");
$route->get("/vip", "WebController:vip");
$route->get("/sitemap.xml", "WebController:sitemap");

$route->post("/vip/cadastrar", "WebController:vipStore");
$route->post("/contato/cadastrar", "WebController:contactStore");

$route->namespace("Application\Controllers\Application");

$route->group("/admin");

$route->get("/", "LoginController:index");
$route->get("/dashboard", "DashboardController:index");
$route->get("/users", "UserController:index");
$route->get("/vips", "VipController:index");
$route->get("/contacts", "ContactController:index");
$route->get("/categories", "CategoryController:index");
$route->get("/posts", "PostController:index");

$route->namespace("Application\Controllers\Site");

$route->group("/ops");

$route->get("/{errcode}", "WebController:error");

$route->dispatch();

if($route->error()){
    $route->redirect("/ops/{$route->error()}");
}

ob_end_flush();
